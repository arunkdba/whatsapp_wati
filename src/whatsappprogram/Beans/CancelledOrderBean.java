package whatsappprogram.Beans;

public class CancelledOrderBean implements Cloneable {

    public String cancelledorder;
    public String partyorderno;
    public String orderdate;
    public String partyname;
    public String count;
    public String weight;
    public String mobileno;
    
    public String canceldateandtime;
    public String cancelleduser;
    public String cancelledreason;

    public String holddateandtime;
    public String holduser;
    public String holdreason;

    public String repuser;
    public String virachetty,suresh,unithead;

    public CancelledOrderBean() {
    }

    public CancelledOrderBean(String cancelledorder, String partyorderno, String orderdate, String partyname, String count, String weight, String mobileno, String canceldateandtime, String cancelleduser, String cancelledreason, String holddateandtime, String holduser, String holdreason, String repuser, String virachetty, String suresh, String unithead) {
        this.cancelledorder = cancelledorder;
        this.partyorderno = partyorderno;
        this.orderdate = orderdate;
        this.partyname = partyname;
        this.count = count;
        this.weight = weight;
        this.mobileno = mobileno;
        this.canceldateandtime = canceldateandtime;
        this.cancelleduser = cancelleduser;
        this.cancelledreason = cancelledreason;
        this.holddateandtime = holddateandtime;
        this.holduser = holduser;
        this.holdreason = holdreason;
        this.repuser = repuser;
        this.virachetty = virachetty;
        this.suresh = suresh;
        this.unithead = unithead;
    }


    public String getRepuser() {
        return repuser;
    }

    public void setRepuser(String repuser) {
        this.repuser = repuser;
    }

    public String getVirachetty() {
        return virachetty;
    }

    public void setVirachetty(String virachetty) {
        this.virachetty = virachetty;
    }

    public String getSuresh() {
        return suresh;
    }

    public void setSuresh(String suresh) {
        this.suresh = suresh;
    }

    public String getUnithead() {
        return unithead;
    }

    public void setUnithead(String unithead) {
        this.unithead = unithead;
    }
    
    
    
    
    public String getHolddateandtime() {
        return holddateandtime;
    }

    public void setHolddateandtime(String holddateandtime) {
        this.holddateandtime = holddateandtime;
    }

    public String getHolduser() {
        return holduser;
    }

    public void setHolduser(String holduser) {
        this.holduser = holduser;
    }

    public String getHoldreason() {
        return holdreason;
    }

    public void setHoldreason(String holdreason) {
        this.holdreason = holdreason;
    }
    
    

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getCancelledorder() {
        return cancelledorder;
    }

    public void setCancelledorder(String cancelledorder) {
        this.cancelledorder = cancelledorder;
    }

    public String getPartyorderno() {
        return partyorderno;
    }

    public void setPartyorderno(String partyorderno) {
        this.partyorderno = partyorderno;
    }

    public String getOrderdate() {
        return orderdate;
    }

    public void setOrderdate(String orderdate) {
        this.orderdate = orderdate;
    }

    public String getPartyname() {
        return partyname;
    }

    public void setPartyname(String partyname) {
        this.partyname = partyname;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCanceldateandtime() {
        return canceldateandtime;
    }

    public void setCanceldateandtime(String canceldateandtime) {
        this.canceldateandtime = canceldateandtime;
    }

    public String getCancelleduser() {
        return cancelleduser;
    }

    public void setCancelleduser(String cancelleduser) {
        this.cancelleduser = cancelleduser;
    }

    public String getCancelledreason() {
        return cancelledreason;
    }

    public void setCancelledreason(String cancelledreason) {
        this.cancelledreason = cancelledreason;
    }

    @Override
    public String toString() {
        return "CancelledOrderBean{" + "cancelledorder=" + cancelledorder + ", partyorderno=" + partyorderno + ", orderdate=" + orderdate + ", partyname=" + partyname + ", count=" + count + ", weight=" + weight + ", mobileno=" + mobileno + ", canceldateandtime=" + canceldateandtime + ", cancelleduser=" + cancelleduser + ", cancelledreason=" + cancelledreason + ", holddateandtime=" + holddateandtime + ", holduser=" + holduser + ", holdreason=" + holdreason + ", repuser=" + repuser + ", virachetty=" + virachetty + ", suresh=" + suresh + ", unithead=" + unithead + '}';
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }


}
