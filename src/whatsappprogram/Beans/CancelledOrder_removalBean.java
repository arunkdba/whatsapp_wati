package whatsappprogram.Beans;

public class CancelledOrder_removalBean implements Cloneable {

    public String orderno;
    public String partyorderno;
    public String orderdate;
    public String partyname;
    public String count;
    public String weight;
    public String mobileno;

    public String removaldate;
    public String removedby;
    public String holdreason;

    public String virachetty, suresh, unithead;

    public CancelledOrder_removalBean() {
    }
    
    

    public CancelledOrder_removalBean(String orderno, String partyorderno, String orderdate, String partyname, String count, String weight, String mobileno, String removaldate, String removedby, String holdreason, String virachetty, String suresh, String unithead) {
        this.orderno = orderno;
        this.partyorderno = partyorderno;
        this.orderdate = orderdate;
        this.partyname = partyname;
        this.count = count;
        this.weight = weight;
        this.mobileno = mobileno;
        this.removaldate = removaldate;
        this.removedby = removedby;
        this.holdreason = holdreason;
        this.virachetty = virachetty;
        this.suresh = suresh;
        this.unithead = unithead;
    }

    
    @Override
    public String toString() {
        return "CancelledOrder_removalBean{" + "orderno=" + orderno + ", partyorderno=" + partyorderno + ", orderdate=" + orderdate + ", partyname=" + partyname + ", count=" + count + ", weight=" + weight + ", mobileno=" + mobileno + ", removaldate=" + removaldate + ", removedby=" + removedby + ", holdreason=" + holdreason + ", virachetty=" + virachetty + ", suresh=" + suresh + ", unithead=" + unithead + '}';
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getPartyorderno() {
        return partyorderno;
    }

    public void setPartyorderno(String partyorderno) {
        this.partyorderno = partyorderno;
    }

    public String getOrderdate() {
        return orderdate;
    }

    public void setOrderdate(String orderdate) {
        this.orderdate = orderdate;
    }

    public String getPartyname() {
        return partyname;
    }

    public void setPartyname(String partyname) {
        this.partyname = partyname;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getRemovaldate() {
        return removaldate;
    }

    public void setRemovaldate(String removaldate) {
        this.removaldate = removaldate;
    }

    public String getRemovedby() {
        return removedby;
    }

    public void setRemovedby(String removedby) {
        this.removedby = removedby;
    }

    public String getHoldreason() {
        return holdreason;
    }

    public void setHoldreason(String holdreason) {
        this.holdreason = holdreason;
    }

    public String getVirachetty() {
        return virachetty;
    }

    public void setVirachetty(String virachetty) {
        this.virachetty = virachetty;
    }

    public String getSuresh() {
        return suresh;
    }

    public void setSuresh(String suresh) {
        this.suresh = suresh;
    }

    public String getUnithead() {
        return unithead;
    }

    public void setUnithead(String unithead) {
        this.unithead = unithead;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

}
