package whatsappprogram.Beans;
public class DyeingCateenStrengthBean implements Cloneable{
    public String tamil,north,staff,total;  // workers counting who are using canteen
    public String mobileno;
    public String date;
    

    public DyeingCateenStrengthBean() {
    }

    public DyeingCateenStrengthBean(String tamil, String north, String staff, String total, String mobileno, String date) {
        this.tamil = tamil;
        this.north = north;
        this.staff = staff;
        this.total = total;
        this.mobileno = mobileno;
        this.date = date;
    }

    
    @Override
    public String toString() {
        return "DyeingCateenStrengthBean{" + "tamil=" + tamil + ", north=" + north + ", staff=" + staff + ", mobileno=" + mobileno + '}';
    }

    
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    public String getTamil() {
        return tamil;
    }

    public void setTamil(String tamil) {
        this.tamil = tamil;
    }

    public String getNorth() {
        return north;
    }

    public void setNorth(String north) {
        this.north = north;
    }

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
