package whatsappprogram.Beans;

public class EDP_staff_Deduction_hrs_Bean  {
    public String date1,mins,amount,totalmins,totalamount,mobileno;

    public EDP_staff_Deduction_hrs_Bean(String date1, String mins, String amount, String totalmins, String totalamount, String mobileno) {
        this.date1 = date1;
        this.mins = mins;
        this.amount = amount;
        this.totalmins = totalmins;
        this.totalamount = totalamount;
        this.mobileno = mobileno;
    }

    public EDP_staff_Deduction_hrs_Bean() {
    }

    @Override
    public String toString() {
        return "EDP_staff_Deduction_hrs_Bean{" + "date1=" + date1 + ", mins=" + mins + ", amount=" + amount + ", totalmins=" + totalmins + ", totalamount=" + totalamount + ", mobileno=" + mobileno + '}';
    }
    

    public String getDate1() {
        return date1;
    }

    public void setDate1(String date1) {
        this.date1 = date1;
    }

    public String getMins() {
        return mins;
    }

    public void setMins(String mins) {
        this.mins = mins;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTotalmins() {
        return totalmins;
    }

    public void setTotalmins(String totalmins) {
        this.totalmins = totalmins;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }
    
}
