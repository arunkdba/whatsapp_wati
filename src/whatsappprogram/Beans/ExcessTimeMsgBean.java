package whatsappprogram.Beans;

public class ExcessTimeMsgBean {
    public String attndate, shift, name, tktno, unit, department, workstarttime, currentworkinghrs, stdworkinghrs, mobileno;
    public int sentstatus;

    public String getAttndate() {
        return attndate;
    }

    public void setAttndate(String attndate) {
        this.attndate = attndate;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTktno() {
        return tktno;
    }

    public void setTktno(String tktno) {
        this.tktno = tktno;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getWorkstarttime() {
        return workstarttime;
    }

    public void setWorkstarttime(String workstarttime) {
        this.workstarttime = workstarttime;
    }

    public String getCurrentworkinghrs() {
        return currentworkinghrs;
    }

    public void setCurrentworkinghrs(String currentworkinghrs) {
        this.currentworkinghrs = currentworkinghrs;
    }

    public String getStdworkinghrs() {
        return stdworkinghrs;
    }

    public void setStdworkinghrs(String stdworkinghrs) {
        this.stdworkinghrs = stdworkinghrs;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobile) {
        this.mobileno = mobile;
    }

    public int getSentstatus() {
        return sentstatus;
    }

    public void setSentstatus(int sentstatus) {
        this.sentstatus = sentstatus;
    }

    
}
