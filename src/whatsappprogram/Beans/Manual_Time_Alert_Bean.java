package whatsappprogram.Beans;

public class Manual_Time_Alert_Bean  {
    public String mobileno,empcode,empname,deptname,unitname,attendancedate;

    public Manual_Time_Alert_Bean() {
    }

    public Manual_Time_Alert_Bean(String mobileno, String empcode, String empname, String deptname, String unitname, String attendancedate) {
        this.mobileno = mobileno;
        this.empcode = empcode;
        this.empname = empname;
        this.deptname = deptname;
        this.unitname = unitname;
        this.attendancedate = attendancedate;
    }

    @Override
    public String toString() {
        return "Manual_Time_Alert_Bean{" + "mobileno=" + mobileno + ", empcode=" + empcode + ", empname=" + empname + ", deptname=" + deptname + ", unitname=" + unitname + ", attendancedate=" + attendancedate + '}';
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getEmpcode() {
        return empcode;
    }

    public void setEmpcode(String empcode) {
        this.empcode = empcode;
    }

    public String getEmpname() {
        return empname;
    }

    public void setEmpname(String empname) {
        this.empname = empname;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    public String getUnitname() {
        return unitname;
    }

    public void setUnitname(String unitname) {
        this.unitname = unitname;
    }

    public String getAttendancedate() {
        return attendancedate;
    }

    public void setAttendancedate(String attendancedate) {
        this.attendancedate = attendancedate;
    }
    
    
 
}
