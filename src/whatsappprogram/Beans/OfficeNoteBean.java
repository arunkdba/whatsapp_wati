package whatsappprogram.Beans;

public class OfficeNoteBean {

    public String officenoteno;
    public String officenotedate;
    public String subject;
    public String touser;
    public String mobileno;  // sendtomobilenp
    public String fromuser;

    public String getOfficenoteno() {
        return officenoteno;
    }

    public void setOfficenoteno(String officenoteno) {
        this.officenoteno = officenoteno;
    }

    public String getOfficenotedate() {
        return officenotedate;
    }

    public void setOfficenotedate(String officenotedate) {
        this.officenotedate = officenotedate;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTouser() {
        return touser;
    }

    public void setTouser(String touser) {
        this.touser = touser;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }


    public String getFromuser() {
        return fromuser;
    }

    public void setFromuser(String fromuser) {
        this.fromuser = fromuser;
    }
    
    
}
