package whatsappprogram.Beans;

public class PF_Warden_AlertBean implements Cloneable {

    public String mobileno;  
    public String noofworkers;

    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getNoofworkers() {
        return noofworkers;
    }

    public void setNoofworkers(String noofworkers) {
        this.noofworkers = noofworkers;
    }

    public PF_Warden_AlertBean(String mobileno, String noofworkers) {
        this.mobileno = mobileno;
        this.noofworkers = noofworkers;
    }

    public PF_Warden_AlertBean() {
    }

}
