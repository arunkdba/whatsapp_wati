package whatsappprogram.Beans;

public class PFwithoutBankDetBean  implements Cloneable {
    public String details1,details2;
    public String mobileno;

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    public PFwithoutBankDetBean(String details1, String details2, String mobileno) {
        this.details1 = details1;
        this.details2 = details2;
        this.mobileno = mobileno;
    }

    public PFwithoutBankDetBean() {
    }
    
    

    public String getDetails1() {
        return details1;
    }

    public void setDetails1(String details1) {
        this.details1 = details1;
    }

    public String getDetails2() {
        return details2;
    }

    public void setDetails2(String details2) {
        this.details2 = details2;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

}
