package whatsappprogram.Beans;

public class PendingWorkBean implements Cloneable {

    public String work1, work2, work3, work4, work5, work6, work7, work8, mobileno , work9,work10;

    public PendingWorkBean(String work1, String work2, String work3, String work4, String work5, String work6, String work7, String work8, String mobileno, String work9, String work10) {
        this.work1 = work1;
        this.work2 = work2;
        this.work3 = work3;
        this.work4 = work4;
        this.work5 = work5;
        this.work6 = work6;
        this.work7 = work7;
        this.work8 = work8;
        this.mobileno = mobileno;
        this.work9 = work9;
        this.work10 = work10;
    }

    public String getWork9() {
        return work9;
    }

    public void setWork9(String work9) {
        this.work9 = work9;
    }

    public String getWork10() {
        return work10;
    }

    public void setWork10(String work10) {
        this.work10 = work10;
    }


    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public PendingWorkBean() {
    }

    @Override
    public String toString() {
        return "PendingWorkBean{" + "work1=" + work1 + ", work2=" + work2 + ", work3=" + work3 + ", work4=" + work4 + ", work5=" + work5 + ", work6=" + work6 + ", work7=" + work7 + ", work8=" + work8 + '}';
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    public String getWork1() {
        return work1;
    }

    public void setWork1(String work1) {
        this.work1 = work1;
    }

    public String getWork2() {
        return work2;
    }

    public void setWork2(String work2) {
        this.work2 = work2;
    }

    public String getWork3() {
        return work3;
    }

    public void setWork3(String work3) {
        this.work3 = work3;
    }

    public String getWork4() {
        return work4;
    }

    public void setWork4(String work4) {
        this.work4 = work4;
    }

    public String getWork5() {
        return work5;
    }

    public void setWork5(String work5) {
        this.work5 = work5;
    }

    public String getWork6() {
        return work6;
    }

    public void setWork6(String work6) {
        this.work6 = work6;
    }

    public String getWork7() {
        return work7;
    }

    public void setWork7(String work7) {
        this.work7 = work7;
    }

    public String getWork8() {
        return work8;
    }

    public void setWork8(String work8) {
        this.work8 = work8;
    }

}
