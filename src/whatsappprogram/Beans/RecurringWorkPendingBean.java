/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package whatsappprogram.Beans;

/**
 *
 * @author admin
 */
public class RecurringWorkPendingBean implements Cloneable {
    public String pendingworks,applink,mobileno;
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    public RecurringWorkPendingBean(String pendingworks, String applink, String mobileno) {
        this.pendingworks = pendingworks;
        this.applink = applink;
        this.mobileno = mobileno;
    }

    public RecurringWorkPendingBean() {
    }

    @Override
    public String toString() {
        return "RecurringWorkPendingBean{" + "pendingworks=" + pendingworks + ", applist=" + applink + ", mobileno=" + mobileno + '}';
    }

    public String getPendingworks() {
        return pendingworks;
    }

    public void setPendingworks(String pendingworks) {
        this.pendingworks = pendingworks;
    }

    public String getApplink() {
        return applink;
    }

    public void setApplink(String applink) {
        this.applink = applink;
    }


    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

}
