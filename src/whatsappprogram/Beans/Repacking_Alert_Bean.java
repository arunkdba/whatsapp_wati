package whatsappprogram.Beans;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Repacking_Alert_Bean implements Cloneable {

    public String orderno, totalbags, totalweight;
    public String mobileno;

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

}
