package whatsappprogram.Beans;

public class Rewinding_UnitPendingBean implements Cloneable {

    public String orderno, receiptdate, issueddate, unitreceiveddate, unitname, mobileno;

    @Override
    public String toString() {
        return "Rewinding_UnitPendingBean{" + "orderno=" + orderno + ", receiptdate=" + receiptdate + ", issueddate=" + issueddate + ", unitreceiveddate=" + unitreceiveddate + ", unitname=" + unitname + ", mobileno=" + mobileno + '}';
    }

    public Rewinding_UnitPendingBean() {
    }

    public Rewinding_UnitPendingBean(String orderno, String receiptdate, String issueddate, String unitreceiveddate, String unitname, String mobileno) {
        this.orderno = orderno;
        this.receiptdate = receiptdate;
        this.issueddate = issueddate;
        this.unitreceiveddate = unitreceiveddate;
        this.unitname = unitname;
        this.mobileno = mobileno;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getReceiptdate() {
        return receiptdate;
    }

    public void setReceiptdate(String receiptdate) {
        this.receiptdate = receiptdate;
    }

    public String getIssueddate() {
        return issueddate;
    }

    public void setIssueddate(String issueddate) {
        this.issueddate = issueddate;
    }

    public String getUnitreceiveddate() {
        return unitreceiveddate;
    }

    public void setUnitreceiveddate(String unitreceiveddate) {
        this.unitreceiveddate = unitreceiveddate;
    }

    public String getUnitname() {
        return unitname;
    }

    public void setUnitname(String unitname) {
        this.unitname = unitname;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

}
