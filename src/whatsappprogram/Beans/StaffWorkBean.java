package whatsappprogram.Beans;

public class StaffWorkBean {
    public String jobcode,givendate,naturejob,username,workgivenby,mobileno;

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getJobecode() {
        return jobcode;
    }

    public void setJobcode(String jobcode) {
        this.jobcode = jobcode;
    }

    public String getGivendate() {
        return givendate;
    }

    public void setGivendate(String givendate) {
        this.givendate = givendate;
    }

    public String getNaturejob() {
        return naturejob;
    }

    public void setNaturejob(String naturejob) {
        this.naturejob = naturejob;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getWorkgivenby() {
        return workgivenby;
    }

    public void setWorkgivenby(String workgivenby) {
        this.workgivenby = workgivenby;
    }
    
}
