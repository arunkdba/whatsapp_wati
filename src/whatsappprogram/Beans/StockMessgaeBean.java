package whatsappprogram.Beans;

import java.sql.Blob;

public class StockMessgaeBean {
    public String image;
    //int sentstatus;
    public String mobileno ,link;
    public String verify_link ;

    
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public String getVerify_link() {
        return verify_link;
    }

    public void setVerify_link(String verify_link) {
        this.verify_link = verify_link;
    }
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
    

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    @Override
    public String toString() {
        return "StockMessgaeBean{" +  ", mobileno=" + mobileno + '}';
    }
  
}
