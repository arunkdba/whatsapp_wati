/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package whatsappprogram.Beans;

/**
 *
 * @author admin
 */
public class WindMillPendingInvoiceBean implements Cloneable {

    public String noofinvoices, mobileno;

    public WindMillPendingInvoiceBean() {
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public WindMillPendingInvoiceBean(String noofinvoices, String mobileno) {
        this.noofinvoices = noofinvoices;
        this.mobileno = mobileno;
    }

    public String getNoofinvoices() {
        return noofinvoices;
    }

    public void setNoofinvoices(String noofinvoices) {
        this.noofinvoices = noofinvoices;
    }

}
