package whatsappprogram.Beans;

public class YarnOrderApprovalBean implements Cloneable {

    public String partyname, orderno, weight;
    public String mobileno;

    public YarnOrderApprovalBean() {
    }

    public YarnOrderApprovalBean(String partyname, String orderno, String weight, String mobileno) {
        this.partyname = partyname;
        this.orderno = orderno;
        this.weight = weight;
        this.mobileno = mobileno;
    }

    @Override
    public String toString() {
        return "YarnOrderApprovalBean{" + "partyname=" + partyname + ", orderno=" + orderno + ", weight=" + weight + ", mobileno=" + mobileno + '}';
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    public String getPartyname() {
        return partyname;
    }

    public void setPartyname(String partyname) {
        this.partyname = partyname;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

}
