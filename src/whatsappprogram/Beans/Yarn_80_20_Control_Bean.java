package whatsappprogram.Beans;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Yarn_80_20_Control_Bean implements Cloneable{

    public String insno, insdate, orderno, reason ,link;
    public String mobileno;

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

}
