package whatsappprogram.Beans;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ins_age_control_bean implements Cloneable{

    public String insno, insdate, orderno, reason;
    public String mobileno;

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

}
