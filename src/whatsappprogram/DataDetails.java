package whatsappprogram;

import whatsappprogram.basicbeans.GRNMsgBean;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import whatsappprogram.Beans.CancelledOrderBean;
import whatsappprogram.Beans.CancelledOrder_removalBean;
import whatsappprogram.Beans.CottonInwardBean;
import whatsappprogram.Beans.DyeingCateenStrengthBean;
import whatsappprogram.Beans.EDP_staff_Deduction_hrs_Bean;
import whatsappprogram.Beans.ExcessTimeMsgBean;
import whatsappprogram.Beans.Manual_Time_Alert_Bean;
import whatsappprogram.Beans.OfficeNoteBean;
import whatsappprogram.Beans.PF_Warden_AlertBean;
import whatsappprogram.Beans.PendingWorkBean;
import whatsappprogram.Beans.RecurringWorkPendingBean;
import whatsappprogram.Beans.Rewinding_UnitPendingBean;
import whatsappprogram.Beans.StaffWorkBean;
import whatsappprogram.Beans.StockMessgaeBean;
import whatsappprogram.Beans.WindMillPendingInvoiceBean;
import whatsappprogram.Beans.YarnOrderApprovalBean;

public class DataDetails {

    public ArrayList<OfficeNoteBean> OfficeNoteList(Connection con) {

        StringBuilder st1 = new StringBuilder(" select OFFICENOTEAUTHENTICATE.officenoteno,to_char(to_date(officenote.officenotedate,'yyyymmdd'),'DD-MM-YYYY') ");
        st1.append(" officenotedate,OFFICENOTEAUTHENTICATE.subject,touser.username touser,touser.mobile tomobile,fromuser.username fromuser,fromuser.mobile from OFFICENOTEAUTHENTICATE ");
        st1.append(" inner join officenote on  officenote.officenoteno=OFFICENOTEAUTHENTICATE.officenoteno and whatsappsentstatus=0 ");
        st1.append(" inner join rawuser touser on OFFICENOTEAUTHENTICATE.empcode = touser.empcode ");
        st1.append(" inner join rawuser fromuser on officenote.fromusercode = fromuser.usercode  ");

        PreparedStatement ps1 = null, ps2 = null;
        ResultSet rs1 = null, rs2 = null;

        ArrayList<OfficeNoteBean> AL = new ArrayList();
        try {
            ps1 = con.prepareStatement(st1.toString());
            rs1 = ps1.executeQuery();

            while (rs1.next()) {
                //System.out.println("data upload");
                OfficeNoteBean SWB = new OfficeNoteBean();
                SWB.setOfficenoteno(rs1.getString(1));
                SWB.setOfficenotedate(rs1.getString(2));
                SWB.setSubject(rs1.getString(3));
                SWB.setTouser(rs1.getString(4));
                SWB.setMobileno(rs1.getString(5));
                SWB.setFromuser(rs1.getString(6));
                AL.add(SWB);
            }
            String sUpdate = "update OFFICENOTEAUTHENTICATE set WHATSAPPSENTSTATUS=1 where WHATSAPPSENTSTATUS=0";
            ps2 = con.prepareStatement(sUpdate);
            ps2.execute();
            ps2.close();
            rs1.close();
            ps1.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return AL;
    }

    public ArrayList<StaffWorkBean> PrepareMessageFromPendingTable(Connection con) {
        StringBuilder st1 = new StringBuilder(" select id,  substr(givendate,7,2) ||'.'|| substr(givendate,5,2) ||'.'|| substr(givendate,1,4) givendate");
        st1.append(" ,NATUREJOB ,raw1.username givenby ,raw2.username,raw2.mobile as mobileno  from scm.staffworkdetails    ");
        st1.append(" inner join scm.rawuser raw1 on raw1.usercode=scm.staffworkdetails.usercode  and sentstatus=0 ");
        st1.append(" inner join scm.rawuser raw2 on raw2.usercode=scm.staffworkdetails.staffcode ");

        PreparedStatement ps1 = null, ps2 = null;
        ResultSet rs1 = null, rs2 = null;

        ArrayList<StaffWorkBean> AL = new ArrayList();
        try {
            ps1 = con.prepareStatement(st1.toString());
            rs1 = ps1.executeQuery();

            while (rs1.next()) {
                //System.out.println("data upload");
                StaffWorkBean SWB = new StaffWorkBean();
                SWB.setJobcode(rs1.getString(1));
                SWB.setGivendate(rs1.getString(2));
                SWB.setNaturejob(rs1.getString(3));
                SWB.setWorkgivenby(rs1.getString(4));
                SWB.setUsername(rs1.getString(5));
                SWB.setMobileno(rs1.getString(6));
                AL.add(SWB);
            }
            String sUpdate = "update scm.staffworkdetails set sentstatus=1 where sentstatus=0";
            ps2 = con.prepareStatement(sUpdate);
            ps2.execute();
            ps2.close();
            rs1.close();
            ps1.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return AL;
    }

    public ArrayList<CancelledOrderBean> PreparePendingOrders(String ordertype, Connection con) throws CloneNotSupportedException {
        StringBuilder sb = new StringBuilder();
        //                 1         2                  3               4               5               6      7  
        sb.append(" select RORDERNO,ORDERDATE,partymaster.partyname,nvl(PARTYORDERNO,'PRDER'),yarncount.COUNTname,WEIGHT,CANCELstatus, ");
        sb.append(" CANCELDATETIME,CANCELREASON,canceluser.username canceluser,HOLDINGDATETIME,HOLDINGREASON,HOLDINGSTATUS,HOLDINGUSER,regularorder.unitcode,canceluser.mobile ");
        sb.append(" ,repuser.mobile repmobile,919566693726 virachetty,919751142668 suresh ,hoduser.mobile ");
        sb.append(" from scm.regularorder inner join scm.partymaster on  scm.regularorder.partycode=scm.partymaster.partycode and regularorder.whatsappsentstatus=0 ");
        if (ordertype.equals("CANCELORDERS")) {
            sb.append(" and scm.regularorder.cancelstatus =1  ");
        } else if (ordertype.equals("HOLDORDERS")) {
            sb.append(" and scm.regularorder.holdingstatus =1 ");
        }
        //sb.append(" and scm.regularorder.rorderno='LM96073' ");
        sb.append(" inner join scm.yarncount on regularorder.countcode=yarncount.countcode ");
        if (ordertype.equals("CANCELORDERS")) {
            sb.append(" inner join scm.rawuser canceluser on regularorder.canceluser=canceluser.usercode   ");
        } else if (ordertype.equals("HOLDORDERS")) {
            sb.append(" inner join scm.rawuser canceluser on regularorder.HOLDINGUSER=canceluser.usercode   ");
        }
        sb.append(" inner join scm.REPRESENTATIVe on regularorder.representativecode=REPRESENTATIVe.code ");
        sb.append(" inner join scm.rawuser repuser on REPRESENTATIVe.userid=repuser.usercode   ");
        sb.append(" inner join unitheaddetails  on  unitheaddetails.unitcode=regularorder.unitcode  ");
        sb.append(" inner join scm.rawuser hoduser  on  hoduser.usercode=unitheaddetails.hodcode  ");

        PreparedStatement ps, ps2;
        ResultSet rs, rs2;
        ArrayList<CancelledOrderBean> CancelledOrderArray = new ArrayList();
        try {
            ps = con.prepareStatement(sb.toString());
            //System.out.println(sb.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                //            System.out.println("orderno "+rs.getString(1));
                CancelledOrderBean GMB = new CancelledOrderBean();
                GMB.setCancelledorder(rs.getString(1));
                GMB.setOrderdate(rs.getString(2));
                GMB.setPartyname(rs.getString(3));
                GMB.setPartyorderno(rs.getString(4));
                GMB.setCount(rs.getString(5));
                GMB.setWeight(rs.getString(6));
                if (ordertype.equals("CANCELORDERS")) {
                    GMB.setCanceldateandtime(rs.getString(8));
                    GMB.setCancelledreason(rs.getString(9));
                    GMB.setCancelleduser(rs.getString(10));
                } else if (ordertype.equals("HOLDORDERS")) {
                    GMB.setHolddateandtime(rs.getString(11));
                    GMB.setHoldreason(rs.getString(12));
                    GMB.setHolduser(rs.getString(14));
                }
                GMB.setMobileno(rs.getString(16));
                CancelledOrderArray.add(GMB);
                // For rep mobile no
                CancelledOrderBean GMB1 = (CancelledOrderBean) GMB.clone();
                GMB1.setMobileno(rs.getString(17));
                CancelledOrderArray.add(GMB1);
                // For vhirachetti mobile
                CancelledOrderBean GMB2 = (CancelledOrderBean) GMB.clone();
                GMB2.setMobileno(rs.getString(18));
                CancelledOrderArray.add(GMB2);
                // For suresh mobile
                CancelledOrderBean GMB3 = (CancelledOrderBean) GMB.clone();
                GMB3.setMobileno(rs.getString(19));
                CancelledOrderArray.add(GMB3);
                // For unit head
                CancelledOrderBean GMB4 = (CancelledOrderBean) GMB.clone();
                GMB4.setMobileno(rs.getString(20));
                CancelledOrderArray.add(GMB4);
            }
            //System.out.println(CancelledOrderArray.get(4).toString());

            rs.close();
            ps.close();

        } catch (SQLException ex) {
            System.out.println("error ");
            ex.printStackTrace();
        }
        System.out.println(CancelledOrderArray.size());
        return CancelledOrderArray;

    }

    public void ClosePendingOrders(String ordertype, Connection con) {
        PreparedStatement ps2 = null;
        try {
            String sUpdate = "";
            if (ordertype.equals("CANCELORDERS")) {
                sUpdate = "update  scm.regularorder set whatsappsentstatus=1 where whatsappsentstatus =0";

            } else if (ordertype.equals("HOLDORDERS")) {
                System.out.println("hold/cancel status updated - 1");
                sUpdate = "update  scm.regularorder set whatsappsentstatus=1 where whatsappsentstatus =0";
            }
            System.out.println("hold/cancel status updated - 2 ");
            ps2 = con.prepareStatement(sUpdate);
            ps2.execute();
            ps2.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public ArrayList<ExcessTimeMsgBean> PrepareMessageFromExcessTable(Connection con) {
        StringBuilder sb = new StringBuilder();
        sb.append(" select attndate ,shift,name,ticketno tktno,unit,department,workstarttime,current_working_hrs currentworkinghrs,stdworkinghrs, ");
        sb.append(" a.empcode,a.username,a.mobile,b.username,b.usercode,b.mobile,sentstatus from TBL_ALERT_WORKINGHOURS ");
        sb.append(" inner join scm.rawuser a on TBL_ALERT_WORKINGHOURS.spo_empcode=a.empcode and sentstatus=0");
        sb.append(" inner join scm.rawuser b on TBL_ALERT_WORKINGHOURS.hod_empcode=b.empcode ");
        sb.append(" and b.usercode not in (1985,5825,4846)");

        PreparedStatement ps;
        ResultSet rs;
        ArrayList<ExcessTimeMsgBean> ExcessWorkerArray = new ArrayList();
        try {
            ps = con.prepareStatement(sb.toString());
            //System.out.println(sb.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                ExcessTimeMsgBean GMB = new ExcessTimeMsgBean();
                GMB.setAttndate(rs.getString(1));
                GMB.setShift(rs.getString(2));
                GMB.setName(rs.getString(3));
                GMB.setTktno(rs.getString(4));
                GMB.setUnit(rs.getString(5));
                GMB.setDepartment(rs.getString(6));
                GMB.setWorkstarttime(rs.getString(7));
                GMB.setCurrentworkinghrs(rs.getString(8));
                GMB.setStdworkinghrs(rs.getString(9));
                GMB.setMobileno(rs.getString(12));
                ExcessWorkerArray.add(GMB);
            }
            String sUpdate = "update TBL_ALERT_WORKINGHOURS set sentstatus=1 where sentstatus=0";
            ps = con.prepareStatement(sUpdate);
            ps.execute();
            rs.close();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ExcessWorkerArray;

    }

    public ArrayList<StockMessgaeBean> StockImageList(Connection con) {
        System.out.println("stock image list 1");
        StringBuilder sb = new StringBuilder();
        sb.append(" select id,imageblob,sentstatus,mobileno from  scm.RMType_Stock_Blob where sentstatus=0 ");

        PreparedStatement ps;
        ResultSet rs;
        ArrayList<StockMessgaeBean> stockMessgaeList = new ArrayList();
        try {
            ps = con.prepareStatement(sb.toString());
            //System.out.println(sb.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                StockMessgaeBean SImage = new StockMessgaeBean();

                int blogLength = (int) rs.getBlob(2).length();
//                SImage.setImage(rs.getBlob(2).getBytes(1, blogLength));

                SImage.setMobileno(rs.getString(4));
                SImage.setVerify_link("hai");
                stockMessgaeList.add(SImage);
                //System.out.println(SImage.toString());
            }

            String sUpdate = "update scm.RMType_Stock_Blob set sentstatus=1 where sentstatus=0";
            ps = con.prepareStatement(sUpdate);
            //ps.execute();
            rs.close();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return stockMessgaeList;

    }

    public ArrayList<StockMessgaeBean> StockImageList1(Connection con) {
        StringBuilder sb = new StringBuilder();
        sb.append(" select id,'http://amarjothi.in:8096/manager/stockimage1.jpg',sentstatus,mobileno,imageblob from  scm.RMType_Stock_Blob where sentstatus=0 ");
        //sb.append(" select id,'C:\\Program Files\\Apache Software Foundation\\Apache Tomcat 8.0.27\\webapps\\manager\\stockimage.jpg',sentstatus,mobileno,imageblob from  scm.RMType_Stock_Blob where sentstatus=0 ");

        PreparedStatement ps;
        ResultSet rs;
        ArrayList<StockMessgaeBean> stockMessgaeList = new ArrayList();
        try {
            ps = con.prepareStatement(sb.toString());
            rs = ps.executeQuery();
            File f = new File("C:\\Program Files\\Apache Software Foundation\\Apache Tomcat 8.0.27\\webapps\\manager\\stockimage1.jpg");
            //File f = new File("d:\\stockimage.jpg");
            if (f.delete()) {
                System.out.println("File deleted ");
            } else {
                System.out.println("File NOT deleted ");
            }
            while (rs.next()) {
                Blob img_blob = rs.getBlob(5);

                //File file = new File("d:\\stockimage.jpg");
                //FileOutputStream outStream = new FileOutputStream(file);
                FileOutputStream outStream = new FileOutputStream("C:\\Program Files\\Apache Software Foundation\\Apache Tomcat 8.0.27\\webapps\\manager\\stockimage1.jpg");
                //FileOutputStream fout = new FileOutputStream("d:\\stockimage.jpg");

                long remaining = img_blob.length();
                int size = 1024 * 1024; // 1MB.
                byte[] buffer = new byte[size];
                long position = 1;     //Write to the outputStream until there is no more data.    
                while (true) {
                    if (remaining < size) {
                        size = (int) remaining;
                    }
                    buffer = img_blob.getBytes(position, size);
                    outStream.write(buffer, 0, size);
                    outStream.flush();
                    position += size;
                    remaining -= size;
                    if (remaining == 0) {
                        break;
                    }
                }
                //Close stream.    
                outStream.close();

                StockMessgaeBean SImage = new StockMessgaeBean();
                SImage.setImage(rs.getString(2));
                SImage.setMobileno("919442240787");
                stockMessgaeList.add(SImage);
                //JMD
                StockMessgaeBean SImage1 = new StockMessgaeBean();
                SImage1.setImage(rs.getString(2));
                SImage1.setMobileno("919894201122");
                stockMessgaeList.add(SImage1);
                //MD
                StockMessgaeBean SImage2 = new StockMessgaeBean();
                SImage2.setImage(rs.getString(2));
                SImage2.setMobileno("919943510001");
                stockMessgaeList.add(SImage2);
                //MDPA senthi
                StockMessgaeBean SImage3 = new StockMessgaeBean();
                SImage3.setImage(rs.getString(2));
                SImage3.setMobileno("917708810001");
                stockMessgaeList.add(SImage3);

            }

            rs.close();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return stockMessgaeList;
    }

    public void CloseStockImageSentList(Connection con) {
        try {
            PreparedStatement ps;
            String sUpdate = " update scm.RMType_Stock_Blob set sentstatus=1 where sentstatus=0 ";
            ps = con.prepareStatement(sUpdate);
            ps.execute();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void CloseMnaualAttendnaceSentList(Connection con) {
        try {
            PreparedStatement ps;
            String sUpdate = " update hrdnew.StaffAttendanceRequest set whatsappsentstatus=1 where whatsappsentstatus=0 ";
            ps = con.prepareStatement(sUpdate);
            ps.execute();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void PreparePendingGRNList(Connection con) {
        StringBuilder sb = new StringBuilder();

        sb.append(" select t.empcode, s.empname, t.mobile,count(1) from ( ");
        sb.append(" select distinct g.grnno, g.grndate, g.invno, g.invdate, p.partyname, spjno, ");
        sb.append(" g.inspection , g.grnauthstatus, g.mrsauthusercode, r.username as GrnAuthUser, r.empcode,r.mobile from inventory.Grn g   ");
        sb.append(" inner join scm.rawuser r on r.usercode = decode(g.bunitauth,1,1526,g.mrsauthusercode) ");
        sb.append(" and g.mrsauthusercode>0  and g.grndate>=20211201 and g.grndate<=to_char(sysdate-1,'YYYYMMDD')   and  g.grnauthstatus=0  and g.spjno=0  and g.rejflag=0 ");
        sb.append(" inner join scm.partymaster p on p.partycode = g.sup_code   ");
        sb.append(" and (g.grnno, g.code) not in ");
        sb.append(" ( Select grnno, code from inventory.grn Where grndate>=20211201 and grn.grndate<=to_char(sysdate-1,'YYYYMMDD') and  grn.grnauthstatus=0  and grn.spjno=0 and grn.rejflag=1 ) ");
        sb.append(" union all ");
        sb.append(" select distinct g.grnno, g.grndate, g.invno, g.invdate, p.partyname, spjno, ");
        sb.append(" g.inspection , g.grnauthstatus, g.memoauthusercode, r.username as GrnAuthUser, r.empcode,r.mobile from inventory.WorkGrn g   ");
        sb.append(" inner join scm.rawuser r on r.usercode = g.memoauthusercode and g.memoauthusercode>0   ");
        sb.append(" and g.grndate>=20211201 and g.grndate<=to_char(sysdate-1,'YYYYMMDD')  and  g.grnauthstatus=0  and g.spjno=0   ");
        sb.append(" inner join scm.partymaster p on p.partycode = g.sup_code   ");
        sb.append(" )  t ");
        sb.append(" inner join hrdnew.staff s on s.empcode = t.empcode ");
        sb.append(" group by   t.empcode, s.empname,t.mobile ");
        sb.append(" order by 2 ");

        StringBuilder sbInsert = new StringBuilder();
        sbInsert.append(" insert into inventory.whatsapp_grn_pending_list(id,empcode,mobileno,nos) values(inventory.seq_whatsapp_grn_pending_list.nextval,?,?,?) ");

        try {
            PreparedStatement ps, ps1;
            ResultSet rs;
            ps = con.prepareStatement(sb.toString());
            //System.out.println(sb.toString());
            ps1 = con.prepareStatement(sbInsert.toString());
            rs = ps.executeQuery();
            String sEmpname = "";
            int iEmpcode = 0;
            String iMobile = "";
            int noofgrns = 0;
            while (rs.next()) {
                iMobile = rs.getString(3);
                // Mobile no length must 12 ex : 919965341365
                if (iMobile.length() != 12) {
                    continue;
                }
                iEmpcode = rs.getInt(1);
                noofgrns = rs.getInt(4);
                ps1.setInt(1, iEmpcode);
                ps1.setString(2, iMobile);
                ps1.setInt(3, noofgrns);
                ps1.execute();
            }
            rs.close();
            ps.close();
            ps1.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public ArrayList<GRNMsgBean> PrepareMessageFromPendingTableNew(Connection con) {
        StringBuilder sb = new StringBuilder();
        sb.append(" select id,empcode,mobileno,nos,days from inventory.whatsapp_grn_pending_list where sentstatus=0  ");
        PreparedStatement ps;
        ResultSet rs;
        ArrayList<GRNMsgBean> GRNArray = new ArrayList();
        try {
            ps = con.prepareStatement(sb.toString());
            //System.out.println(sb.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                GRNMsgBean GMB = new GRNMsgBean();
                GMB.setId(rs.getInt(1));
                GMB.setMobileno(rs.getString(3));
                GMB.setNos(rs.getString(4));
                GMB.setDays(rs.getString(5));
                GRNArray.add(GMB);
            }
            String sUpdate = "update inventory.whatsapp_grn_pending_list set sentstatus=1";
            ps = con.prepareStatement(sUpdate);
            ps.execute();
            rs.close();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return GRNArray;
    }

    public ArrayList<CancelledOrder_removalBean> CancelledOrderDetails(Connection con) {
        StringBuilder sb = new StringBuilder();
        //                    1      2                  3                4              5              6            7
        sb.append(" select RORDERNO,ORDERDATE,partymaster.partyname,PARTYORDERNO,yarncount.COUNTname,WEIGHT,canceluser.username, ");
        sb.append(" cancelledregularorder.entrydate  as canceldate, ");
        //                    9                              10                          11                       12
        sb.append(" canceluser.mobile cancelledusermobile,repuser.mobile repmobile ,918838599696 virachetty,918838533696 suresh ");
        //                    13                   14            
        sb.append(" ,hoduser.mobile hodmobile , 919442240787 arun ");
        sb.append(" from scm.cancelledregularorder inner join scm.partymaster on  cancelledregularorder.partycode=partymaster.partycode  and cancelledregularorder.whatsappsentstatus=0 ");
//sb.append(" --and rorderno='LM95874' ");
        sb.append(" inner join scm.yarncount on scm.cancelledregularorder.countcode=yarncount.countcode ");
        sb.append(" inner join scm.rawuser canceluser on scm.cancelledregularorder.cancelusercode=canceluser.usercode   ");
        sb.append(" inner join scm.REPRESENTATIVe on scm.cancelledregularorder.representativecode=REPRESENTATIVe.code ");
        sb.append(" inner join scm.rawuser repuser on scm.REPRESENTATIVe.userid=repuser.usercode   ");
        sb.append(" inner join scm.rawuser hoduser  on  hoduser.unitheadcode=cancelledregularorder.unitcode ");

        PreparedStatement ps;
        ResultSet rs;
        ArrayList<CancelledOrder_removalBean> OrderArray = new ArrayList();
        try {
            ps = con.prepareStatement(sb.toString());
            //System.out.println(sb.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                CancelledOrder_removalBean RB = new CancelledOrder_removalBean();
                RB.setOrderno(rs.getString(1));
                RB.setOrderdate(rs.getString(2));
                RB.setPartyname(rs.getString(3));
                RB.setPartyorderno(rs.getString(4));
                RB.setCount(rs.getString(5));
                RB.setWeight(rs.getString(6));
                RB.setRemovedby(rs.getString(7));
                RB.setRemovaldate(rs.getString(8));
                RB.setHoldreason("Check with Rep");
                RB.setMobileno(rs.getString(9));
                OrderArray.add(RB);

                //for rep
                CancelledOrder_removalBean RB1 = (CancelledOrder_removalBean) RB.clone();
                RB1.setMobileno(rs.getString(10));
                OrderArray.add(RB1);
                // For vhirachetti mobile
                CancelledOrder_removalBean RB2 = (CancelledOrder_removalBean) RB.clone();
                RB2.setMobileno(rs.getString(11));
                OrderArray.add(RB2);
                // For suresh mobile
                CancelledOrder_removalBean RB3 = (CancelledOrder_removalBean) RB.clone();
                RB3.setMobileno(rs.getString(12));
                OrderArray.add(RB3);
                // For unit head
                CancelledOrder_removalBean RB4 = (CancelledOrder_removalBean) RB.clone();
                RB4.setMobileno(rs.getString(13));
                OrderArray.add(RB4);
            }
            String sUpdate = "update scm.cancelledregularorder set whatsappsentstatus=1 where whatsappsentstatus=0 ";
            ps = con.prepareStatement(sUpdate);
            ps.execute();
            rs.close();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return OrderArray;
    }

    public ArrayList<CottonInwardBean> CottonInwardList(Connection con) {
        StringBuilder sb = new StringBuilder();
        String users = "";

        sb.append(" select  partymaster.partyname,bales,dcno,to_char(to_date(cottongi.gidate,'yyyymmdd'),'DD-MM-YYYY')   || ' ' ||  timein as inward_datetime ");
        sb.append(" from scm.cottongi inner join partymaster on cottongi.accode =partymaster.partycode and whatsappsentstatus=0 and partymaster.partycode not in ('949292','1020655') ");

        users = " select usercode,username,mobile from  rawuser where  whatsapp_cottongi_inward=1  ";

        PreparedStatement ps, ps1;
        ResultSet rs, rs1;
        ArrayList<CottonInwardBean> CottonArray = new ArrayList();

        try {
            ps = con.prepareStatement(sb.toString());
            ps1 = con.prepareCall(users, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            //System.out.println(sb.toString());
            rs = ps.executeQuery();
            rs1 = ps1.executeQuery();

            while (rs.next()) {
                CottonInwardBean RB = new CottonInwardBean();
                // for each transaction message sent to all mobile nos
                rs1.beforeFirst();
                rs1.next();
                RB.setSupplier_name(rs.getString(1));
                RB.setBales(rs.getString(2));
                RB.setDcno(rs.getString(3));
                RB.setInward_datetime(rs.getString(4));
                RB.setMobileno(rs1.getString(3));
                System.out.println(RB.toString());
                CottonArray.add(RB);
                while (rs1.next()) {
                    CottonInwardBean RB1 = (CottonInwardBean) RB.clone();
                    RB1.setMobileno(rs1.getString(3));
                    System.out.println(RB1.toString());
                    CottonArray.add(RB1);
                }
            }
            rs.close();
            ps.close();
            rs1.close();
            ps1.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return CottonArray;
    }

    public void CloseCottonInwardList(Connection con) {
        try {
            PreparedStatement ps;
            String sUpdate = "update scm.cottongi set whatsappsentstatus=1 where whatsappsentstatus=0 and accode not in ('949292','1020655')";
            ps = con.prepareStatement(sUpdate);
            ps.execute();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public ArrayList<Rewinding_UnitPendingBean> RewindingPendingAtUnitList(Connection con) {
        StringBuilder sb = new StringBuilder();
        String users = "";

        sb.append(" Select GodownToMillReProcess.ReProcessNo, GodownToMillReProcess.OrderNo, to_char(to_date(GodownToMillReProcess.ReceiptDate,'yyyymmdd'),'DD-MM-YYYY') as ReceiptDate, ");
        sb.append(" to_char(to_date(GodownToWindingIssue.IssueToWindingDate,'yyyymmdd'),'DD-MM-YYYY') as issuedate, to_Char(GodownToWindingIssue.WindingReceiptDateTime, 'DD-MM-YYYY') as WindingReceiptDate, ");
        sb.append(" to_Date(to_Char(SysDate, 'YYYYMMDD'), 'YYYYMMDD') - to_Date(GodownToWindingIssue.IssueToWindingDate, 'YYYYMMDD') as Days ");
        sb.append(" ,GodownToWindingIssue.issueunitcode, unit.unitname,hoduser.username,hoduser.mobile,processuser.empcode,processuser.username,processuser.mobile ");
        sb.append(" from dispatch.GodownToMillReProcess ");
        sb.append(" Inner Join dispatch.GodownToWindingIssue on GodownToWindingIssue.ReProcessNo = GodownToMillReProcess.ReProcessNo and GodownToMillReProcess.WindingCompleteStatus = 0 ");
        sb.append(" inner join scm.unit on GodownToWindingIssue.issueunitcode= unit.unitcode ");
        sb.append(" inner join scm.rawuser hoduser on  hoduser.unitheadcode=GodownToWindingIssue.issueunitcode ");
        sb.append(" inner join scm.rawuser processuser on  processuser.unitcode=GodownToWindingIssue.issueunitcode and processuser.processfollowerflag=1 ");

        PreparedStatement ps;
        ResultSet rs;
        ArrayList<Rewinding_UnitPendingBean> RWArray = new ArrayList();

        try {
            ps = con.prepareStatement(sb.toString());
            //System.out.println(sb.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                Rewinding_UnitPendingBean RB = new Rewinding_UnitPendingBean();
                RB.setOrderno(rs.getString(2));
                //orderno,receiptdate,issueddate,unitreceiveddate,unitname,mobileno;
                RB.setReceiptdate(rs.getString(3));
                RB.setIssueddate(rs.getString(4));
                RB.setUnitreceiveddate(rs.getString(5));
                RB.setUnitname(rs.getString(8));
                RB.setMobileno(rs.getString(10));
                RWArray.add(RB);

                //for rep
                Rewinding_UnitPendingBean RB1 = (Rewinding_UnitPendingBean) RB.clone();
                RB1.setMobileno(rs.getString(13));
                RWArray.add(RB1);
            }
            rs.close();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return RWArray;
    }

    public ArrayList<YarnOrderApprovalBean> YarnOrderAppovalList(Connection con) {
        StringBuilder sb = new StringBuilder();
        String users = "";

        sb.append(" select partyname,rorderno,weight,repuser.mobile repmobile ,918838599696 virachetty,918838533696 suresh from regularorder  ");
        sb.append(" inner join partymaster on regularorder.partycode=partymaster.partycode and auth_whatsappsentstatus =0  and orderdate>=20220301");
        sb.append(" inner join REPRESENTATIVe on regularorder.representativecode=REPRESENTATIVe.code ");
        sb.append(" inner join rawuser repuser on REPRESENTATIVe.userid=repuser.usercode   ");

        PreparedStatement ps;
        ResultSet rs;
        ArrayList<YarnOrderApprovalBean> YOAArray = new ArrayList();

        try {
            ps = con.prepareStatement(sb.toString());
            //System.out.println(sb.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                YarnOrderApprovalBean RB = new YarnOrderApprovalBean();
                RB.setPartyname(rs.getString(1));
                RB.setOrderno(rs.getString(2));
                RB.setWeight(rs.getString(3));
                RB.setMobileno(rs.getString(4));
                YOAArray.add(RB);

                // For vhirachetti mobile
                YarnOrderApprovalBean RB1 = (YarnOrderApprovalBean) RB.clone();
                RB1.setMobileno(rs.getString(5));
                YOAArray.add(RB1);
                //for suresh
                YarnOrderApprovalBean RB3 = (YarnOrderApprovalBean) RB.clone();
                RB3.setMobileno(rs.getString(6));
                YOAArray.add(RB3);

            }
            rs.close();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return YOAArray;
    }

    public ArrayList<DyeingCateenStrengthBean> DyeingCanteenStrength(Connection con) {
        StringBuilder sb = new StringBuilder();
        String users = "";
        sb.append(" create  or replace view dyestrenght as ( ");
        sb.append(" select suM(tamilcount) a ,sum(orissacount) o,5 as staffcount,919442240787 as mobileno,to_char(sysdate-1,'dd.mm.yyyy' ) curdate  ,suM(tamilcount)+sum(orissacount)+5  as total  from ( ");
        sb.append(" Select count(distinct CurrentAttendance.empcode) as tamilcount,0 as orissacount, 0 as staffcount ");
        sb.append(" from CurrentAttendance@dyehrd ");
        sb.append(" Inner join Schemeapprentice@dyehrd on Schemeapprentice.EmpCode = CurrentAttendance.EmpCode where InDate >= to_char(sysdate-3,'yyyymmdd') ");
        sb.append(" and Status  in (1,7) and hostelcode>0 ");
        sb.append(" union all ");
        sb.append(" Select 0 as  tamilcount , count(distinct CurrentAttendance.empcode) as orissacount,0 as staffcount  ");
        sb.append(" from CurrentAttendance@dyehrd  ");
        sb.append(" Inner join ContractApprentice@dyehrd on ContractApprentice.EmpCode = CurrentAttendance.EmpCode where InDate >= to_char(sysdate-3,'yyyymmdd') ");
        sb.append(" and Status  in (1,7) and hostelcode>0 ");
        sb.append(" )) ");

        PreparedStatement ps;
        ResultSet rs;
        ArrayList<DyeingCateenStrengthBean> DWSArray = new ArrayList();

        try {
            ps = con.prepareStatement(sb.toString());
            //System.out.println(sb.toString());
            ps.executeQuery();
            rs = ps.executeQuery("select * from dyestrenght");

            while (rs.next()) {
                System.out.println(rs.getString(1));
                DyeingCateenStrengthBean RB = new DyeingCateenStrengthBean();
                RB.setTamil(rs.getString(1));
                RB.setNorth(rs.getString(2));
                RB.setStaff(rs.getString(3));
                RB.setMobileno(rs.getString(4));
                RB.setDate(rs.getString(5));
                RB.setTotal(rs.getString(6));
                DWSArray.add(RB);

                // For smb mobile
                DyeingCateenStrengthBean RB1 = (DyeingCateenStrengthBean) RB.clone();
                RB1.setMobileno("916379906200");
                DWSArray.add(RB1);

                //for Canteen karrupasamy
                //DyeingCateenStrengthBean RB2 = (DyeingCateenStrengthBean) RB.clone();
                //RB2.setMobileno("918526022323");
                //DWSArray.add(RB2);
                //for GM dyeing unit
                DyeingCateenStrengthBean RB3 = (DyeingCateenStrengthBean) RB.clone();
                RB3.setMobileno("916381009080");
                DWSArray.add(RB3);

                //for Velu
                DyeingCateenStrengthBean RB4 = (DyeingCateenStrengthBean) RB.clone();
                RB4.setMobileno("917867915609");
                DWSArray.add(RB4);

                //for Pradeep
                DyeingCateenStrengthBean RB5 = (DyeingCateenStrengthBean) RB.clone();
                RB5.setMobileno("917806928884");
                DWSArray.add(RB5);

            }
            rs.close();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return DWSArray;
    }

    //PendingInvoiceCount
    public ArrayList<WindMillPendingInvoiceBean> PendingInvoiceCount(Connection con) {
        StringBuilder sb = new StringBuilder();
        String users = "";

        sb.append(" select count(1) from  maintenance.WindPower_Invoice where  WindPower_Invoice.IAAUTHSTATUS is null and  to_char(WindPower_Invoice.InvoiceDate,'yyyymmdd') >=20220501 ");

        PreparedStatement ps;
        ResultSet rs;
        ArrayList<WindMillPendingInvoiceBean> DWSArray = new ArrayList();

        try {
            ps = con.prepareStatement(sb.toString());
            System.out.println(sb.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                if (rs.getInt(1) > 0) {
                    WindMillPendingInvoiceBean RB = new WindMillPendingInvoiceBean();
                    RB.setNoofinvoices(rs.getString(1));
                    RB.setMobileno("919442240787");
                    DWSArray.add(RB);

                    // For IA Srnivasan mobile
                    WindMillPendingInvoiceBean RB1 = (WindMillPendingInvoiceBean) RB.clone();
                    RB1.setMobileno("918012860913");
                    DWSArray.add(RB1);
                }
            }
            rs.close();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return DWSArray;
    }

    public void CloseYarnOrderClosingList(Connection con) {
        try {
            PreparedStatement ps;
            String sUpdate = "update scm.regularorder set auth_whatsappsentstatus=1 where auth_whatsappsentstatus=0";
            ps = con.prepareStatement(sUpdate);
            ps.execute();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
//WorkPendingList

    public ArrayList<PendingWorkBean> WorkPendingList(Connection con) {
        StringBuilder sb = new StringBuilder();
        String users = "";

        sb.append(" select distinct hrdnew.PUNCHINGCHECKDETAILSNEW.entrydate,hrdnew.PUNCHINGCHECKDETAILSNEW.empcode,reasonfornonpunching.reasonname as work,  ");
        sb.append(" rawuser.username,rawuser.mobile from hrdnew.PUNCHINGCHECKDETAILSNEW  ");
        sb.append(" inner join hrdnew.reasonfornonpunching on hrdnew.PUNCHINGCHECKDETAILSNEW.PUNCHINGCHECKCODE =hrdnew.reasonfornonpunching.nonpunchreasoncode   ");
        sb.append(" inner join hrdnew.staff on staff.empcode=PUNCHINGCHECKDETAILSNEW.empcode and staff.empcode not in (191541,193639)   ");
        sb.append(" inner join rawuser on staff.empcode=rawuser.empcode and   rawuser.usercode not in (1981, 1985,2) order by mobile  ");

        PreparedStatement ps;
        ResultSet rs;
        ArrayList<PendingWorkBean> WPArray = new ArrayList();

        try {
            ps = con.prepareStatement(sb.toString());
            //System.out.println(sb.toString());
            rs = ps.executeQuery();
            String Nxtmobileno = "";
            PendingWorkBean RB = null;
            int workcount = 1;  // Work count for same mobile no
            int newmobilentry = 0;
            while (rs.next()) {
                String sMobileno = rs.getString(5);
                //System.out.println("mobile no " + rs.getString(5));
                //if (!sMobileno.equals("919442240787")) {
                //    continue;
                //}
                if (!Nxtmobileno.equals("") && sMobileno.equals(Nxtmobileno)) {
                    workcount++;
                    if (workcount == 2) {
                        RB.setWork2(rs.getString(3));
                    } else if (workcount == 3) {
                        RB.setWork3(rs.getString(3));
                    } else if (workcount == 4) {
                        RB.setWork4(rs.getString(3));
                    } else if (workcount == 5) {
                        RB.setWork5(rs.getString(3));
                    } else if (workcount == 6) {
                        RB.setWork6(rs.getString(3));
                    } else if (workcount == 7) {
                        RB.setWork7(rs.getString(3));
                    } else if (workcount == 8) {
                        RB.setWork8(rs.getString(3));
                    } else if (workcount == 9) {
                        RB.setWork9(rs.getString(3));
                    } else if (workcount == 10) {
                        RB.setWork10(rs.getString(3));
                    }

                } else {
                    if (newmobilentry == 0) {
                        RB = new PendingWorkBean();
                        newmobilentry = 1;
                    } else {
                        WPArray.add(RB);
                        RB = new PendingWorkBean();
                        workcount = 1;
                    }
                    RB.setWork1(rs.getString(3));
                    RB.setMobileno(rs.getString(5));

                }
                Nxtmobileno = rs.getString(5);
            }
            WPArray.add(RB);
            System.out.println(WPArray.get(0));
            rs.close();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return WPArray;
    }
//PendingRecurringWork

    public ArrayList<RecurringWorkPendingBean> PendingRecurringWork(Connection con) {
        StringBuilder sb = new StringBuilder();
        String users = "";

        sb.append(" select mobile,count(1) from ( ");
        sb.append(" Select rawuser.usercode,rawuser.username,rawuser.mobile  from RECURRING_WORK_Pool rwt Inner join alpa.Division on Division.DivisionCode = rwt.DivisionCode and rwt.isDelete = 0 Inner    ");
        sb.append(" join hrdnew.DepartMent on DepartMent.DeptCode = rwt.DeptCode Inner join Recurring_Company_Master rcm on rcm.Id = rwt.CompanyName Inner join Recurring_WorkType_Master rwtm on rwtm.Id =  ");
        sb.append(" rwt.workTypeCode Inner join Recurring_WorkPriority_Master rwpm on rwpm.Id = rwt.PriorityCode Inner join Recurring_WorkFrequency_Master rwfm on rwfm.Id = rwt.FrequencyCode Inner join  ");
        sb.append(" Recurring_WorkFrequency_Type on Recurring_WorkFrequency_Type.Id = rwfm.FrequencyType Inner join RECURRING_WORK_User_Pool rwu on rwu.WorkId = rwt.Id And rwu.CompleteStatus!= 1 Inner join Rawuser  ");
        sb.append(" on Rawuser.usercode = rwu.UserCode And rwu.UserCode!=-1 Inner join Recuring_Work_Approval_Time a on a.Id = rwu.Appr_Time Inner join Recuring_Work_Approval_Time b on b.Id = rwu.Hold_Time Inner  ");
        sb.append(" join Recuring_Work_Approval_Time c on c.Id = rwu.Exten_Time Inner join RECURRING_WORKFLOW_MASTER rwflm on rwflm.Id = rwu.WorkFlow_Code Where rwt.CompleteStatus != 1 And  ");
        sb.append(" round(to_date(StartDate,'YYYYMMDD')-to_date(to_char(Sysdate,'YYYYMMDD'),'YYYYMMDD'))<=0 And (rwu.IMMEDIATE_INTIMATION = 1 Or rwu.WorkFlow_Priority = ( ");
        sb.append(" Select Min(WorkFlow_Priority) From  ");
        sb.append(" RECURRING_WORK_User_Pool Where WorkId = rwt.Id And UserCode!=-1 And Completestatus!=1 ) ))   group by mobile ");
        System.out.println(sb.toString());
        PreparedStatement ps;
        ResultSet rs;
        ArrayList<RecurringWorkPendingBean> RWPArray = new ArrayList();

        try {
            ps = con.prepareStatement(sb.toString());
            //System.out.println(sb.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                RecurringWorkPendingBean RB = new RecurringWorkPendingBean();
                RB.setPendingworks(rs.getString(2));
                RB.setMobileno(rs.getString(1));
                RB.setApplink("http://172.16.2.14:8084/RecurringWork");

                RWPArray.add(RB);
            }
            rs.close();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return RWPArray;
    }

    public ArrayList<PF_Warden_AlertBean> PFAlertJob(Connection con) {
        StringBuilder sb = new StringBuilder();
        String users = "";

        sb.append(" select mobile from rawuser where pf_warden_alert=1 ");

        //System.out.println(sb.toString());
        PreparedStatement ps;
        ResultSet rs;
        ArrayList<PF_Warden_AlertBean> PWAArray = new ArrayList();

        try {
            ps = con.prepareStatement(sb.toString());
            //System.out.println(sb.toString());
            rs = ps.executeQuery();

            while (rs.next()) {
                PF_Warden_AlertBean PWA = new PF_Warden_AlertBean();
                PWA.setNoofworkers("5");
                PWA.setMobileno(rs.getString(1));
                PWAArray.add(PWA);
            }
            rs.close();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return PWAArray;
    }

    //DeductionAlert
    public ArrayList<EDP_staff_Deduction_hrs_Bean> DeductionAlert(Connection con) {
        StringBuilder sb = new StringBuilder();
        String users = "";

        sb.append("  with  ");
        sb.append(" uptodatecost as  ");
        sb.append(" (select empcode,sum(mins) totmins,sum(amount) totamount from ");
        sb.append(" hrdstaffmonitoring where substr(deddate,0,6)=to_char(sysdate,'yyyymm') group by empcode) ");

        sb.append(" select to_char(to_date(deddate,'yyyymmdd'),'DD/MM/YYYY') AS deddate,totmins,totamount,mins,amount,rawuser.mobile mobileno from uptodatecost ");
        sb.append(" inner join hrdstaffmonitoring on  hrdstaffmonitoring.empcode=uptodatecost.empcode ");
        sb.append(" inner join rawuser on hrdstaffmonitoring.empcode=rawuser.empcode ");
        sb.append(" where whatsappsentstatus=0	        ");

        System.out.println(sb.toString());
        PreparedStatement ps;
        ResultSet rs;
        ArrayList<EDP_staff_Deduction_hrs_Bean> EDPAlertArray = new ArrayList();

        try {
            ps = con.prepareStatement(sb.toString());

            rs = ps.executeQuery();

            while (rs.next()) {
                EDP_staff_Deduction_hrs_Bean STDB = new EDP_staff_Deduction_hrs_Bean();

                STDB.setDate1(rs.getString(1));
                STDB.setTotalmins(rs.getString(2));
                STDB.setTotalamount(rs.getString(3));
                STDB.setMins(rs.getString(4));
                STDB.setAmount(rs.getString(5));
                STDB.setMobileno(rs.getString(6));
                EDPAlertArray.add(STDB);
            }
            rs.close();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return EDPAlertArray;
    }

    public ArrayList<Manual_Time_Alert_Bean> ManualAttenanceAlert(Connection con) {
        StringBuilder sb = new StringBuilder();
        String users = "";

        sb.append("  Select EmpName,StaffAttendanceRequest.EmpCode,DeptName,UnitName,AttendanceDate,'919442240787' mobileno  ");
        sb.append("  from hrdnew.StaffAttendanceRequest  ");
        sb.append("  Inner join hrdnew.Staff on Staff.EmpCOde = StaffAttendanceRequest.EmpCode and whatsappsentstatus=0  ");
        sb.append("  Inner join hrdnew.Department on Department.DeptCode = StaffAttendanceRequest.DeptCode  ");
        sb.append("  Inner join hrdnew.Unit on Unit.UnitCode = StaffAttendanceRequest.UnitCode  ");

    //    System.out.println(sb.toString());
        PreparedStatement ps;
        ResultSet rs;
        ArrayList<Manual_Time_Alert_Bean> MAAAlertArray = new ArrayList();

        try {
            ps = con.prepareStatement(sb.toString());

            rs = ps.executeQuery();

            while (rs.next()) {
                Manual_Time_Alert_Bean MAAB = new Manual_Time_Alert_Bean();
                MAAB.setEmpname(rs.getString(1));
                MAAB.setEmpcode(rs.getString(2));
                MAAB.setDeptname(rs.getString(3));
                MAAB.setUnitname(rs.getString(4));
                MAAB.setAttendancedate(rs.getString(5));
                MAAB.setMobileno(rs.getString(6));
                MAAAlertArray.add(MAAB);
            }
            rs.close();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return MAAAlertArray;
    }

    public String FindNextRunTime(String sMinute, String sHour, String sDOF, String sMonth, String sDayOfWeek, String StdRunDate, Connection con) {
        ResultSet rs;
        String DateAddtion = "sysdate ";
        Timestamp dDate = null;
        String simpleDate = "";
        int iHour = 0;
        if (!sHour.equals("*")) {
            iHour = Integer.parseInt(sHour);
        }
        int iMinute = Integer.parseInt(sMinute);
        /*if (!sDOF.equals('*') && !sMinute.equals('*') && !sHour.equals('*')) {
            DateAddtion =  "" ;
        }*/
        if (sMinute.equals("*") && sHour.equals("*") && sDOF.equals("*") && sDayOfWeek.equals("*")) {
        } else if (!StdRunDate.equals("")) {
            //DateAddtion = " to_date('" + StdRunDate + "','yyyy.mm.dd hh24:mi:ss' )+ " + 1 + " + " + iHour + "/(24) +  " + iMinute + "/(24*60) ";
            DateAddtion = " to_date(to_char(sysdate,'yyyy.mm.dd'),'yyyy.mm.dd') + " + 1 + " + " + iHour + "/(24) +  " + iMinute + "/(24*60) ";
        } else if (!StdRunDate.equals("") && sHour.equals("*")) {
            DateAddtion += " + interval '" + sMinute + "' minute ";
        } else if (!sMinute.equals('*')) {
            DateAddtion += " + interval '" + sMinute + "' minute ";
        } else if (!sHour.equals('*')) {
            DateAddtion += "+ interval '" + sHour + "' hour ";
        }

        String qryNextRun = " select " + DateAddtion + " from dual";
        //System.out.println(qryNextRun);
        try {
            PreparedStatement psNextRun = con.prepareStatement(qryNextRun);
            rs = psNextRun.executeQuery();
            rs.next();
            dDate = rs.getTimestamp(1);
            simpleDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dDate);
            psNextRun.close();
            rs.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return simpleDate;
    }

}
