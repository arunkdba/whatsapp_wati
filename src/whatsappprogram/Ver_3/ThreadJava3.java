package whatsappprogram.Ver_3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import whatsappprogram.Beans.Adolesent_Fitness_Bean;
import whatsappprogram.Beans.CancelledOrderBean;
import whatsappprogram.Beans.CancelledOrder_removalBean;
import whatsappprogram.Beans.CottonInwardBean;
import whatsappprogram.Beans.DyeingCateenStrengthBean;
import whatsappprogram.Beans.EDP_staff_Deduction_hrs_Bean;
import whatsappprogram.Beans.ExcessTimeMsgBean;
import whatsappprogram.Beans.Manual_Time_Alert_Bean;
import whatsappprogram.Beans.OfficeNoteBean;
import whatsappprogram.Beans.PFAdditionBean;
import whatsappprogram.Beans.PF_Warden_AlertBean;
import whatsappprogram.Beans.PFwithoutBankDetBean;
import whatsappprogram.Beans.PendingWorkBean;
import whatsappprogram.Beans.RecurringWorkPendingBean;
import whatsappprogram.Beans.RecurringWorkPending_ver3_Bean;
import whatsappprogram.Beans.Repacking_Alert_Bean;
import whatsappprogram.Beans.Rewinding_UnitPendingBean;
import whatsappprogram.Beans.StaffWorkBean;
import whatsappprogram.Beans.StockMessgaeBean;
import whatsappprogram.Beans.WindMillPendingInvoiceBean;
import whatsappprogram.Beans.YarnOrderApprovalBean;
import whatsappprogram.Beans.Yarn_80_20_Control_Bean;
import whatsappprogram.Beans.ins_age_control_bean;
import whatsappprogram.Ver_3.DataDetails_ver_3;
import whatsappprogram.basicbeans.GRNMsgBean;
import whatsappprogram.basic.OracleConnection;
import whatsappprogram.basicbeans.TemplateBean;
import whatsappprogram.basicbeans.TemplateParameterBean;
import whatsappprogram.Ver_3.WhatsappSendClassNew_Ver_3;

public class ThreadJava3 extends Thread {

    public static String sMinute;
    public static String sHour;
    public static String sDOF;
    public static String sMonth;
    public static String sDayOfWeek;
    public static String StdRunDate;
    static ArrayList<TemplateParameterBean> TPBarray = new ArrayList();
    static TemplateBean TB = new TemplateBean();
    static int itemplateid;
    DataDetails_ver_3 dataDetails_ver_3 = new DataDetails_ver_3();

    static int count = 0;
    public static Connection con = OracleConnection.getConnection();
    WhatsappSendClassNew_Ver_3 WSC;

    // WhatsappSendClass WSC;
    ThreadJava3() {
        super("Thread");
        start();
    }

    public void run() {
        System.out.println("Starting Count  Ver 3 " + count++);
        try {
            //con = OracleConnection.getConnection();
            WSC = new WhatsappSendClassNew_Ver_3();
            //WSC = new WhatsappSendClass();
            while (true) {
                // if 1 Whatsapp between 9.30 to 21.00
                int iCode = getCurrnetTime();
                System.out.println("Loop Count  " + count++);
                PrepareData(iCode);
                Thread.sleep(100000); //2 Mins
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            OracleConnection.closeConnection(con);
        }
    }

    /*
    4 excess_working_hrs_1 -  pending
    5 excess_working_hrs_x - correction needed
    6 user_grn_approval_pending_plain_1 - ok
    8 rawstock_alert_version1-x - ok
    9 staff_work_details_ver2 - ok
    10 order_cancel_ver2 - ok
    12 order_hold_ver1 - ok
    13 officenote_intimation_ver1 - ok
    14 cotton_inward_ver1 - OK
    15 Gotton_inward from Gobitex - under develo
    16 Rewinding order alert message -  completed
    17 Order approval from HO - completed
    18. dyeing_canteen_length - completed
    19. wind_invoice_auth_pending_ia  - completed
    20. recurring_pending_ver1 - completed
    21. ward_pf_alert_ver1 - completed
    22. daily_work_list_ver1 - completed
    23. edp_staff_worked_hrs_ver2 - completed
    24. manual_time_alert_ver1 - completed
    25. pf_without_bank_ver1 - completed
    26. recurring_pending_ver1 - completed
    27. Adolescent - completed
    28. yarn_age_removal_ver1 - completed
    29. yarn_80_20_remove_alert_ver1 completed
    30. repacking_alert_ver1 
     */
    /**
     * Templates ready run based on the time
     *
     * @param con - Database connectivity
     * @param TimeConfig - Based on time which template to be run
     */
    private void PrepareData(int TimeConfig) {
        //DataDetails dataDetails_ver_3 = new DataDetails();
        // Check with whatsapp_template_master table 
        // which templated to be run at the specific time . Details available in that table
        PreparedStatement ps1 = null, ps2 = null;
        ResultSet rs1 = null, rs2 = null;
        // tables contains wati url and authorization token
        String st = "select url,authorization from whatsapp_config";
        // tables contains all tempates which was approved by Whatsapp manager in Facebook thro wati.io
        // Select templates based  sysdate > nextruntime to execute
        // update nextruntime  after every execution
        String st1 = "select template_id,template_name,broadcast_name,time_minute,time_hour,dayofmonth,month,dayofweek,creationdate,messagetype,lastruntime,nextruntime,standardruntime "
                + " from whatsapp_template_master where (sysdate> nextruntime or standardruntime is null ) and enabled=1 and timeconfigcode = " + TimeConfig;

        try {
            ps1 = con.prepareStatement(st1);
            rs1 = ps1.executeQuery();
            while (rs1.next()) {
                System.out.println(rs1.getString(2));
                if (rs1.getString(2).equals("user_grn_approval_pending_plain_1")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));

                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));

                    dataDetails_ver_3.PreparePendingGRNList();
                    ArrayList<GRNMsgBean> GRNList = dataDetails_ver_3.PrepareMessageFromPendingTableNew();
                    // ## Send Whatsapp message
                    ////WhatsappSendClassNew WSC = new WhatsappSendClassNew();
                    WSC.SendWhatsapp(TB, GRNList);

                    //updateNextRunTime
                    updateNextRunTime();
                } else if (rs1.getString(2).equals("excess_working_hrs_x")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));

                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));

                    //PreparePendingGRNList();
                    ArrayList<ExcessTimeMsgBean> ExcessTimeWorkerList = dataDetails_ver_3.PrepareMessageFromExcessTable();
                    //WhatsappSendClassNew WSC = new WhatsappSendClassNew();
                    WSC.SendWhatsapp(TB, ExcessTimeWorkerList);

                } else if (rs1.getString(2).equals("rawstock_alert_version2-x")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));

                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));

                    //PreparePendingGRNList();
                    ArrayList<StockMessgaeBean> StockImageList = dataDetails_ver_3.StockImageList();
                    //WhatsappSendClassNew WSC = new WhatsappSendClassNew();
                    WSC.SendWhatsapp(TB, StockImageList);

                } else if (rs1.getString(2).equals("rawstock_image_ver2")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));

                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));

                    //PreparePendingGRNList();
                    ArrayList<StockMessgaeBean> StockImageList = dataDetails_ver_3.StockImageList1();
                    if (StockImageList.size() > 0) {
                        if (WSC.SendWhatsappNew(TB, StockImageList)) {
                            dataDetails_ver_3.CloseStockImageSentList();
                        }
                    }
                    //arun      
                } else if (rs1.getString(2).equals("staff_work_details_ver2")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));

                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<StaffWorkBean> WorkList = dataDetails_ver_3.PrepareMessageFromPendingTable();
                    //WhatsappSendClassNew WSC = new WhatsappSendClassNew();
                    if (WorkList.size() > 0) {
                        WSC.SendWhatsapp(TB, WorkList);
                    }

                    //updateNextRunTime
                    updateNextRunTime();

                } else if (rs1.getString(2).equals("order_cancel_ver2")) {  // Reqeust message for cancellation
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));

                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<CancelledOrderBean> PendingOrderList = dataDetails_ver_3.PreparePendingOrders("CANCELORDERS");
                    //WhatsappSendClassNew WSC = new WhatsappSendClassNew();
                    if (PendingOrderList.size() > 0) {
                        if (WSC.SendWhatsappNew(TB, PendingOrderList)) {
                            dataDetails_ver_3.ClosePendingOrders("CANCELORDERS");
                        }
                    }
                    //updateNextRunTime
                    updateNextRunTime();

                } else if (rs1.getString(2).equals("order_hold_ver1")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<CancelledOrderBean> PendingOrderList = dataDetails_ver_3.PreparePendingOrders("HOLDORDERS");
                    //WhatsappSendClassNew WSC = new WhatsappSendClassNew();
                    if (PendingOrderList.size() > 0) {
                        if (WSC.SendWhatsappNew(TB, PendingOrderList)) {
                            dataDetails_ver_3.ClosePendingOrders("HOLDORDERS");
                        }
                    }

                    //updateNextRunTime
                    updateNextRunTime();

                } else if (rs1.getString(2).equals("officenote_intimation_ver1")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<OfficeNoteBean> officeNoteList = dataDetails_ver_3.OfficeNoteList();
                    //WhatsappSendClassNew WSC = new WhatsappSendClassNew();
                    if (officeNoteList.size() > 0) {
                        WSC.SendWhatsapp(TB, officeNoteList);
                    }

                    //updateNextRunTime
                    updateNextRunTime();

                } else if (rs1.getString(2).equals("order_cancelled_ver2")) {  // Mesage after Order Cancellaton
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<CancelledOrder_removalBean> CancelOrderList = dataDetails_ver_3.CancelledOrderDetails();
                    //WhatsappSendClassNew WSC = new WhatsappSendClassNew();
                    if (CancelOrderList.size() > 0) {
                        WSC.SendWhatsapp(TB, CancelOrderList);
                    }
                    //updateNextRunTime
                    updateNextRunTime();

                } else if (rs1.getString(2).equals("cotton_inward_ver2")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<CottonInwardBean> CottonList = dataDetails_ver_3.CottonInwardList();
                    if (CottonList.size() > 0) {
                        if (WSC.SendWhatsappNew(TB, CottonList)) {
                            dataDetails_ver_3.CloseCottonInwardList();
                        }
                    }
                    //updateNextRunTime
                    updateNextRunTime();
                } else if (rs1.getString(2).equals("rewind_unitpending_")) {  // Mesage after Order Cancellaton
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<Rewinding_UnitPendingBean> RewindingList = dataDetails_ver_3.RewindingPendingAtUnitList();
                    if (RewindingList.size() > 0) {
                        WSC.SendWhatsappNew(TB, RewindingList);
                    }
                    //updateNextRunTime
                    updateNextRunTime();
                } else if (rs1.getString(2).equals("order_approval_ver1")) {  // Mesage after Order Cancellaton
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<YarnOrderApprovalBean> YOAList = dataDetails_ver_3.YarnOrderAppovalList();
                    if (YOAList.size() > 0) {
                        System.out.println(YOAList.get(0).partyname);
                        if (WSC.SendWhatsappNew(TB, YOAList)) {
                            dataDetails_ver_3.CloseYarnOrderClosingList();
                        }
                    }
                    //updateNextRunTime
                    updateNextRunTime();
                } else if (rs1.getString(2).equals("daily_work_list_ver2")) {
                    itemplateid = rs1.getInt(1);
                    System.out.println("tempate id " + itemplateid);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TemplateBean TB = new TemplateBean();
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));

                    ArrayList<PendingWorkBean> WPList = dataDetails_ver_3.WorkPendingList();

                    if (WPList.size() > 0) {
                        WSC.SendWhatsappNew(TB, WPList);
                    }
                    //updateNextRunTime
                    updateNextRunTime();
                } else if (rs1.getString(2).equals("dyeing_canteen_strength_ver1")) {  // Mesage after Order Cancellaton
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<DyeingCateenStrengthBean> WPList = dataDetails_ver_3.DyeingCanteenStrength();  // Last 3 days distinct details
                    System.out.println(WPList.size());
                    if (WPList.size() > 0) {
                        WSC.SendWhatsappNew(TB, WPList);
                    }
                    //updateNextRunTime
                    updateNextRunTime();
                } else if (rs1.getString(2).equals("wind_invoice_auth_pending_ia")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<WindMillPendingInvoiceBean> PIList = dataDetails_ver_3.PendingInvoiceCount();
                    if (PIList.size() > 0) {
                        WSC.SendWhatsappNew(TB, PIList);
                    }
                    //updateNextRunTime
                    updateNextRunTime();
                } else if (rs1.getString(2).equals("recurring_pending_ver1")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<RecurringWorkPendingBean> RWPList = dataDetails_ver_3.PendingRecurringWork();
                    if (RWPList.size() > 0) {
                        WSC.SendWhatsappNew(TB, RWPList);
                    }
                    //updateNextRunTime
                    updateNextRunTime();
                } else if (rs1.getString(2).equals("recurring_pending_ver5")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<RecurringWorkPending_ver3_Bean> RWPList = dataDetails_ver_3.PendingRecurringWork_ver4();
                    if (RWPList.size() > 0) {
                        WSC.SendWhatsappNew(TB, RWPList);
                    }
                    //updateNextRunTime
                    updateNextRunTime();
                } else if (rs1.getString(2).equals("yarn_age_removal_ver1")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<ins_age_control_bean> IACList = dataDetails_ver_3.PendingAgeApproval_PendingList();
                    if (IACList.size() > 0) {
                        if (WSC.SendWhatsappNew(TB, IACList)) {
                            dataDetails_ver_3.ClosePendingAgeRemoval();
                        }
                    }
                    updateNextRunTime();
                } else if (rs1.getString(2).equals("yarn_80_20_remove_alert_ver1")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<Yarn_80_20_Control_Bean> IACList = dataDetails_ver_3.Pending80_20Approval_List();
                    if (IACList.size() > 0) {
                        if (WSC.SendWhatsappNew(TB, IACList)) {
                            dataDetails_ver_3.Close80_20PendingOrders();
                        }
                    }
                    updateNextRunTime();
                } else if (rs1.getString(2).equals("ward_pf_alert_ver")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<PF_Warden_AlertBean> PFAlertList = dataDetails_ver_3.PFAlertJob();
                    if (PFAlertList.size() > 0) {
                        WSC.SendWhatsappNew(TB, PFAlertList);
                    }
                    //updateNextRunTime
                    updateNextRunTime();
                } else if (rs1.getString(2).equals("edp_staff_worked_hrs_ver2")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<EDP_staff_Deduction_hrs_Bean> PFAlertList = dataDetails_ver_3.DeductionAlert();
                    if (PFAlertList.size() > 0) {
                        WSC.SendWhatsappNew(TB, PFAlertList);
                    }
                    //updateNextRunTime
                    updateNextRunTime();
                } else if (rs1.getString(2).equals("manual_time_alert_ver1")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<Manual_Time_Alert_Bean> MAAlertList = dataDetails_ver_3.ManualAttenanceAlert();
                    if (MAAlertList.size() > 0) {
                        if (WSC.SendWhatsappNew(TB, MAAlertList)) {
                            dataDetails_ver_3.CloseMnaualAttendnaceSentList();
                        }
                    }
                    updateNextRunTime();
                } else if (rs1.getString(2).equals("repacking_alert_ver1")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<Repacking_Alert_Bean> RepackAlertList = dataDetails_ver_3.RepackingAlert();
                    if (RepackAlertList.size() > 0) {
                        if (WSC.SendWhatsappNew(TB, RepackAlertList)) {
                            dataDetails_ver_3.CloseRepackingAlertList();
                        }
                    }
                    updateNextRunTime();
                } else if (rs1.getString(2).equals("pf_without_bank_ver2")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<PFwithoutBankDetBean> PFNoBankList = dataDetails_ver_3.getPFwithoutBankDetails();
                    if (PFNoBankList.size() > 0) {
                        WSC.SendWhatsappNew(TB, PFNoBankList);
                    }
                    updateNextRunTime();
                } else if (rs1.getString(2).equals("pf_addtion_ver1")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<PFAdditionBean> PFAdditionList = dataDetails_ver_3.getPFAdditionDetails();
                    if (PFAdditionList.size() > 0) {
                        WSC.SendWhatsappNew(TB, PFAdditionList);
                    }
                    updateNextRunTime();
                } else if (rs1.getString(2).equals("adolescent_pending_list_ver1")) {
                    itemplateid = rs1.getInt(1);
                    // Get formated date for next scheduled run
                    getTimeDetails(rs1);
                    // Get template parameters
                    getTemplateParameters(rs1.getInt(1));
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<Adolesent_Fitness_Bean> PFAdditionList = dataDetails_ver_3.getAdolesentPendingList();
                    if (PFAdditionList.size() > 0) {
                        WSC.SendWhatsappNew(TB, PFAdditionList);
                    }
                    updateNextRunTime();
                }

            }
            ps1.close();
            rs1.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                ps1.close();
                ps2.close();
                rs1.close();
                rs2.close();
            } catch (Exception ee) {
            }
        }
    }

    public int getCurrnetTime() {
        StringBuilder SB = new StringBuilder();
        SB.append(" select code from whatsapp_time_config ");
        SB.append(" where to_date(to_char(sysdate,'HH24:MI'),'HH24:MI') between to_date(daystarttime,'HH24:MI') and  ");
        SB.append(" to_date(dayendtime,'HH24:MI') ");
        int iCode = 0;
        ResultSet rs = null;
        try {
            PreparedStatement ps = con.prepareStatement(SB.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                iCode = rs.getInt(1);
            }
            ps.close();
            rs.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return iCode;
    }

    public void getTimeDetails(ResultSet rs1) {
        try {
            sMinute = rs1.getString(4);
            sHour = rs1.getString(5);
            sDOF = rs1.getString(6);
            sMonth = rs1.getString(7);
            sDayOfWeek = rs1.getString(8);
            StdRunDate = "";
            if (rs1.getDate(13) != null) {
                StdRunDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs1.getDate(13));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void updateNextRunTime() throws Exception {
        // Find next run time
        String nextDate = FindNextRunTime(sMinute, sHour, sDOF, sMonth, sDayOfWeek, StdRunDate);
        //System.out.println(nextDate);
        //updateNextRunTime
        StringBuilder qryNextRun = new StringBuilder();
        //String qryNextRun = " update  whatsapp_template_master set lastruntime= sysdate , nextruntime = to_date('" + nextDate + "','yyyy-mm-dd hh24:mi:ss') where template_id =" + itemplateid;
        //System.out.println(qryNextRun);
        qryNextRun.append("    update  whatsapp_template_master set lastruntime= ");
        qryNextRun.append("    case when to_char(sysdate,'D')<7   then    sysdate ");
        qryNextRun.append("    else  sysdate+1   end, ");
        qryNextRun.append("    nextruntime =  ");
        qryNextRun.append("    case when to_char(sysdate,'D')<7   then    to_date('" + nextDate + "','yyyy-mm-dd hh24:mi:ss') ");
        qryNextRun.append("    else  to_date('" + nextDate + "','yyyy-mm-dd hh24:mi:ss')+1   end ");
        qryNextRun.append("    where template_id =" + itemplateid);

        PreparedStatement psNextRun = con.prepareStatement(qryNextRun.toString());
        psNextRun.executeUpdate();
        psNextRun.close();

    }

    public String FindNextRunTime(String sMinute, String sHour, String sDOF, String sMonth, String sDayOfWeek, String StdRunDate) {
        ResultSet rs;
        String DateAddtion = "sysdate ";
        Timestamp dDate = null;
        String simpleDate = "";
        int iHour = 0;
        if (!sHour.equals("*")) {
            iHour = Integer.parseInt(sHour);
        }
        int iMinute = Integer.parseInt(sMinute);
        /*if (!sDOF.equals('*') && !sMinute.equals('*') && !sHour.equals('*')) {
            DateAddtion =  "" ;
        }*/
        if (sMinute.equals("*") && sHour.equals("*") && sDOF.equals("*") && sDayOfWeek.equals("*")) {
        } else if (!StdRunDate.equals("")) {
            //DateAddtion = " to_date('" + StdRunDate + "','yyyy.mm.dd hh24:mi:ss' )+ " + 1 + " + " + iHour + "/(24) +  " + iMinute + "/(24*60) ";
            DateAddtion = " to_date(to_char(sysdate,'yyyy.mm.dd'),'yyyy.mm.dd') + " + 1 + " + " + iHour + "/(24) +  " + iMinute + "/(24*60) ";
        } else if (!StdRunDate.equals("") && sHour.equals("*")) {
            DateAddtion += " + interval '" + sMinute + "' minute ";
        } else if (!sMinute.equals('*')) {
            DateAddtion += " + interval '" + sMinute + "' minute ";
        } else if (!sHour.equals('*')) {
            DateAddtion += "+ interval '" + sHour + "' hour ";
        }

        String qryNextRun = " select " + DateAddtion + " from dual";
        //System.out.println(qryNextRun);
        try {
            PreparedStatement psNextRun = con.prepareStatement(qryNextRun);
            rs = psNextRun.executeQuery();
            rs.next();
            dDate = rs.getTimestamp(1);
            simpleDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dDate);
            psNextRun.close();
            rs.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return simpleDate;
    }

    public void getTemplateParameters(int template_id) throws Exception {
        PreparedStatement ps2 = null;
        ResultSet rs2 = null;

        String stpara = " select id,template_id,parametername from whatsapp_template_master_para where  template_id = " + template_id;
        ps2 = con.prepareStatement(stpara);
        rs2 = ps2.executeQuery();
        TPBarray.clear();
        while (rs2.next()) {
            TemplateParameterBean TPB = new TemplateParameterBean();
            TPB.setName(rs2.getString(3));
            TPBarray.add(TPB);
        }
        rs2.close();
        ps2.close();

    }
}
