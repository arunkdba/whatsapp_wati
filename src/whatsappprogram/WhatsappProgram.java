package whatsappprogram;

/*
Whatssappprogram
TemplateBean - Have all templates
TemplateParamterBean  -  Have parameters for each template
WhatsapaSendClass  - 
 */
import whatsappprogram.basicbeans.GRNMsgBean;
import whatsappprogram.basicbeans.TemplateParameterBean;
import whatsappprogram.basicbeans.TemplateBean;
import whatsappprogram.basic.OracleConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.DatatypeConverter;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import whatsappprogram.Beans.CancelledOrderBean;
import whatsappprogram.Beans.CancelledOrder_removalBean;
import whatsappprogram.Beans.CottonInwardBean;
import whatsappprogram.Beans.DyeingCateenStrengthBean;
import whatsappprogram.Beans.ExcessTimeMsgBean;
import whatsappprogram.Beans.OfficeNoteBean;
import whatsappprogram.Beans.PendingWorkBean;
import whatsappprogram.Beans.Rewinding_UnitPendingBean;
import whatsappprogram.Beans.StaffWorkBean;
import whatsappprogram.Beans.StockMessgaeBean;
import whatsappprogram.Beans.WindMillPendingInvoiceBean;
import whatsappprogram.Beans.YarnOrderApprovalBean;
import whatsappprogram.Beans.RecurringWorkPendingBean;
import whatsappprogram.Beans.PF_Warden_AlertBean;

/**
 *
 * @author Arunkumar
 */
public class WhatsappProgram {

    public static void main(String[] args) {
        try {
            new ThreadJava();
            //SendWhatsapp();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            System.out.println("app closed");
        }
    }
}

class ThreadJava extends Thread {

    static int count = 0;
    Connection con;
    WhatsappSendClassNew WSC;

    // WhatsappSendClass WSC;
    ThreadJava() {
        super("Thread");
        start();
    }

    public void run() {
        System.out.println("Starting Count  " + count++);
        try {
            con = OracleConnection.getConnection();
            WSC = new WhatsappSendClassNew(con);
            //WSC = new WhatsappSendClass(con);
            while (true) {
                // if 1 Whatsapp between 9.30 to 21.00
                int iCode = getCurrnetTime(con);
                System.out.println("Loop Count  " + count++);
                PrepareData(con, iCode);
                Thread.sleep(100000); //2 Mins
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            OracleConnection.closeConnection(con);
        }
    }

    /*
    4 excess_working_hrs_1 -  pending
    5 excess_working_hrs_x - correction needed
    6 user_grn_approval_pending_plain_1 - ok
    8 rawstock_alert_version1-x - ok
    9 staff_work_details_ver2 - ok
    10 order_cancel_ver2 - ok
    12 order_hold_ver1 - ok
    13 officenote_intimation_ver1 - ok
    14 cotton_inward_ver1 - OK
    15 Gotton_inward from Gobitex - under develo
    16 Rewinding order alert message -  completed
    17 Order approval from HO - completed
    18. dyeing_canteen_length - completed
    19. wind_invoice_auth_pending_ia  - completed
    20. recurring_pending_ver1 - completed
    21. ward_pf_alert_ver1 - completed
    22. daily_work_list_ver1 - pending
     */
    /**
     * Templates ready run based on the time
     *
     * @param con - Database connectivity
     * @param TimeConfig - Based on time which template to be run
     */
    private void PrepareData(Connection con, int TimeConfig) {
        DataDetails dataDetails = new DataDetails();
        // Check with whatsapp_template_master table 
        // which templated to be run at the specific time . Details available in that table
        PreparedStatement ps1 = null, ps2 = null;
        ResultSet rs1 = null, rs2 = null;
        // tables contains wati url and authorization token
        String st = "select url,authorization from whatsapp_config";
        // tables contains all tempates which was approved by Whatsapp manager in Facebook thro wati.io
        // Select templates based  sysdate > nextruntime to execute
        // update nextruntime  after every execution
        String st1 = "select template_id,template_name,broadcast_name,time_minute,time_hour,dayofmonth,month,dayofweek,creationdate,messagetype,lastruntime,nextruntime,standardruntime "
                + " from whatsapp_template_master where (sysdate> nextruntime or standardruntime is null ) and enabled=1 and timeconfigcode = " + TimeConfig;

        try {
            ps1 = con.prepareStatement(st1);
            rs1 = ps1.executeQuery();
            while (rs1.next()) {
                System.out.println(rs1.getString(2));
                if (rs1.getString(2).equals("user_grn_approval_pending_plain_1")) {
                    int itemplateid = rs1.getInt(1);
                    String sMinute = rs1.getString(4);
                    String sHour = rs1.getString(5);
                    String sDOF = rs1.getString(6);
                    String sMonth = rs1.getString(7);
                    String sDayOfWeek = rs1.getString(8);
                    String StdRunDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs1.getDate(13));
                    // Get template parameters
                    String stpara = " select id,template_id,parametername from whatsapp_template_master_para where  template_id = " + itemplateid;
                    ps2 = con.prepareStatement(stpara);
                    rs2 = ps2.executeQuery();
                    ArrayList<TemplateParameterBean> TPBarray = new ArrayList();
                    while (rs2.next()) {
                        TemplateParameterBean TPB = new TemplateParameterBean();
                        TPB.setName(rs2.getString(3));
                        TPBarray.add(TPB);
                    }
                    rs2.close();
                    ps2.close();
                    TemplateBean TB = new TemplateBean();
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));

                    dataDetails.PreparePendingGRNList(con);
                    ArrayList<GRNMsgBean> GRNList = dataDetails.PrepareMessageFromPendingTableNew(con);
                    // ## Send Whatsapp message
                    ////WhatsappSendClassNew WSC = new WhatsappSendClassNew(con);
                    WSC.SendWhatsapp(TB, GRNList);

                    // Find next run time
                    String nextDate = dataDetails.FindNextRunTime(sMinute, sHour, sDOF, sMonth, sDayOfWeek, StdRunDate, con);
                    ////System.out.println(nextDate);
                    //updateNextRunTime
                    String qryNextRun = " update  whatsapp_template_master set lastruntime= sysdate , nextruntime = to_date('" + nextDate + "','yyyy-mm-dd hh24:mi:ss') where template_id =" + itemplateid;
                    //System.out.println(qryNextRun);
                    PreparedStatement psNextRun = con.prepareStatement(qryNextRun);
                    psNextRun.executeUpdate();
                    psNextRun.close();
                } else if (rs1.getString(2).equals("excess_working_hrs_x")) {
                    int itemplateid = rs1.getInt(1);
                    String sMinute = rs1.getString(4);
                    String sHour = rs1.getString(5);
                    String sDOF = rs1.getString(6);
                    String sMonth = rs1.getString(7);
                    String sDayOfWeek = rs1.getString(8);
                    // XXXX
                    String StdRunDate = "";
                    if (rs1.getDate(13) != null) {
                        StdRunDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs1.getDate(13));
                    }
                    // Get template parameters
                    String stpara = " select id,template_id,parametername from whatsapp_template_master_para where  template_id = " + itemplateid;
                    ps2 = con.prepareStatement(stpara);
                    rs2 = ps2.executeQuery();
                    ArrayList<TemplateParameterBean> TPBarray = new ArrayList();
                    while (rs2.next()) {
                        TemplateParameterBean TPB = new TemplateParameterBean();
                        TPB.setName(rs2.getString(3));
                        TPBarray.add(TPB);
                    }
                    rs2.close();
                    ps2.close();

                    TemplateBean TB = new TemplateBean();
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));

                    //PreparePendingGRNList();
                    ArrayList<ExcessTimeMsgBean> ExcessTimeWorkerList = dataDetails.PrepareMessageFromExcessTable(con);
                    //WhatsappSendClassNew WSC = new WhatsappSendClassNew(con);
                    WSC.SendWhatsapp(TB, ExcessTimeWorkerList);

                } else if (rs1.getString(2).equals("rawstock_alert_version2-x")) {
                    int itemplateid = rs1.getInt(1);
                    String sMinute = rs1.getString(4);
                    String sHour = rs1.getString(5);
                    String sDOF = rs1.getString(6);
                    String sMonth = rs1.getString(7);
                    String sDayOfWeek = rs1.getString(8);
                    // XXXX
                    String StdRunDate = "";
                    if (rs1.getDate(13) != null) {
                        StdRunDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs1.getDate(13));
                    }
                    // Get template parameters
                    String stpara = " select id,template_id,parametername from whatsapp_template_master_para where  template_id = " + itemplateid;
                    ps2 = con.prepareStatement(stpara);
                    rs2 = ps2.executeQuery();
                    ArrayList<TemplateParameterBean> TPBarray = new ArrayList();
                    while (rs2.next()) {
                        TemplateParameterBean TPB = new TemplateParameterBean();
                        TPB.setName(rs2.getString(3));
                        TPBarray.add(TPB);
                    }
                    rs2.close();
                    ps2.close();

                    TemplateBean TB = new TemplateBean();
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));

                    //PreparePendingGRNList();
                    ArrayList<StockMessgaeBean> StockImageList = dataDetails.StockImageList(con);
                    //WhatsappSendClassNew WSC = new WhatsappSendClassNew(con);
                    WSC.SendWhatsapp(TB, StockImageList);

                } else if (rs1.getString(2).equals("rawstock_image_ver2")) {
                    int itemplateid = rs1.getInt(1);
                    String sMinute = rs1.getString(4);
                    String sHour = rs1.getString(5);
                    String sDOF = rs1.getString(6);
                    String sMonth = rs1.getString(7);
                    String sDayOfWeek = rs1.getString(8);
                    // XXXX
                    String StdRunDate = "";
                    if (rs1.getDate(13) != null) {
                        StdRunDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs1.getDate(13));
                    }
                    // Get template parameters
                    String stpara = " select id,template_id,parametername from whatsapp_template_master_para where  template_id = " + itemplateid;
                    ps2 = con.prepareStatement(stpara);
                    rs2 = ps2.executeQuery();
                    ArrayList<TemplateParameterBean> TPBarray = new ArrayList();
                    while (rs2.next()) {
                        TemplateParameterBean TPB = new TemplateParameterBean();
                        TPB.setName(rs2.getString(3));
                        TPBarray.add(TPB);
                    }
                    rs2.close();
                    ps2.close();

                    TemplateBean TB = new TemplateBean();
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));

                    //PreparePendingGRNList();
                    ArrayList<StockMessgaeBean> StockImageList = dataDetails.StockImageList1(con);
                    if (StockImageList.size() > 0) {
                        if (WSC.SendWhatsappNew(TB, StockImageList)) {
                            dataDetails.CloseStockImageSentList(con);
                        }
                    }

                } else if (rs1.getString(2).equals("staff_work_details_ver2")) {
                    int itemplateid = rs1.getInt(1);
                    String sMinute = rs1.getString(4);
                    String sHour = rs1.getString(5);
                    String sDOF = rs1.getString(6);
                    String sMonth = rs1.getString(7);
                    String sDayOfWeek = rs1.getString(8);
                    String StdRunDate = "";
                    if (rs1.getDate(13) != null) {
                        StdRunDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs1.getDate(13));
                    }

                    // Get template parameters
                    String stpara = " select id,template_id,parametername from whatsapp_template_master_para where  template_id = 9 ";
                    ps2 = con.prepareStatement(stpara);
                    rs2 = ps2.executeQuery();
                    ArrayList<TemplateParameterBean> TPBarray = new ArrayList();
                    while (rs2.next()) {
                        TemplateParameterBean TPB = new TemplateParameterBean();
                        TPB.setName(rs2.getString(3));
                        TPBarray.add(TPB);
                    }
                    rs2.close();
                    ps2.close();

                    TemplateBean TB = new TemplateBean();
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<StaffWorkBean> WorkList = dataDetails.PrepareMessageFromPendingTable(con);
                    //WhatsappSendClassNew WSC = new WhatsappSendClassNew(con);
                    if (WorkList.size() > 0) {
                        WSC.SendWhatsapp(TB, WorkList);
                    }

                    // Find next run time
                    String nextDate = dataDetails.FindNextRunTime(sMinute, sHour, sDOF, sMonth, sDayOfWeek, StdRunDate, con);
                    //System.out.println(nextDate);
                    //updateNextRunTime
                    String qryNextRun = " update  whatsapp_template_master set lastruntime= sysdate , nextruntime = to_date('" + nextDate + "','yyyy-mm-dd hh24:mi:ss') where template_id =" + itemplateid;
                    //System.out.println(qryNextRun);
                    PreparedStatement psNextRun = con.prepareStatement(qryNextRun);
                    psNextRun.executeUpdate();
                    psNextRun.close();

                } else if (rs1.getString(2).equals("order_cancel_ver2")) {  // Reqeust message for cancellation
                    int itemplateid = rs1.getInt(1);
                    String sMinute = rs1.getString(4);
                    String sHour = rs1.getString(5);
                    String sDOF = rs1.getString(6);
                    String sMonth = rs1.getString(7);
                    String sDayOfWeek = rs1.getString(8);
                    String StdRunDate = "";
                    if (rs1.getDate(13) != null) {
                        StdRunDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs1.getDate(13));
                    }

                    // Get template parameters
                    String stpara = " select id,template_id,parametername from whatsapp_template_master_para where  template_id = 10 ";
                    ps2 = con.prepareStatement(stpara);
                    rs2 = ps2.executeQuery();
                    ArrayList<TemplateParameterBean> TPBarray = new ArrayList();
                    while (rs2.next()) {
                        TemplateParameterBean TPB = new TemplateParameterBean();
                        TPB.setName(rs2.getString(3));
                        TPBarray.add(TPB);
                    }
                    rs2.close();
                    ps2.close();

                    TemplateBean TB = new TemplateBean();
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<CancelledOrderBean> PendingOrderList = dataDetails.PreparePendingOrders("CANCELORDERS", con);
                    //WhatsappSendClassNew WSC = new WhatsappSendClassNew(con);
                    if (PendingOrderList.size() > 0) {
                        if (WSC.SendWhatsappNew(TB, PendingOrderList)) {
                            dataDetails.ClosePendingOrders("CANCELORDERS", con);
                        }
                    }
                    // Find next run time
                    String nextDate = dataDetails.FindNextRunTime(sMinute, sHour, sDOF, sMonth, sDayOfWeek, StdRunDate, con);
                    //System.out.println(nextDate);
                    //updateNextRunTime
                    String qryNextRun = " update  whatsapp_template_master set lastruntime= sysdate , nextruntime = to_date('" + nextDate + "','yyyy-mm-dd hh24:mi:ss') where template_id =" + itemplateid;
                    //System.out.println(qryNextRun);
                    PreparedStatement psNextRun = con.prepareStatement(qryNextRun);
                    psNextRun.executeUpdate();
                    psNextRun.close();

                } else if (rs1.getString(2).equals("order_hold_ver1")) {
                    int itemplateid = rs1.getInt(1);
                    String sMinute = rs1.getString(4);
                    String sHour = rs1.getString(5);
                    String sDOF = rs1.getString(6);
                    String sMonth = rs1.getString(7);
                    String sDayOfWeek = rs1.getString(8);
                    String StdRunDate = "";
                    if (rs1.getDate(13) != null) {
                        StdRunDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs1.getDate(13));
                    }

                    // Get template parameters
                    String stpara = " select id,template_id,parametername from whatsapp_template_master_para where  template_id = 12 ";
                    ps2 = con.prepareStatement(stpara);
                    rs2 = ps2.executeQuery();
                    ArrayList<TemplateParameterBean> TPBarray = new ArrayList();
                    while (rs2.next()) {
                        TemplateParameterBean TPB = new TemplateParameterBean();
                        TPB.setName(rs2.getString(3));
                        TPBarray.add(TPB);
                    }
                    rs2.close();
                    ps2.close();
                    TemplateBean TB = new TemplateBean();
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<CancelledOrderBean> PendingOrderList = dataDetails.PreparePendingOrders("HOLDORDERS", con);
                    //WhatsappSendClassNew WSC = new WhatsappSendClassNew(con);
                    if (PendingOrderList.size() > 0) {
                        if (WSC.SendWhatsappNew(TB, PendingOrderList)) {
                            dataDetails.ClosePendingOrders("HOLDORDERS", con);
                        }
                    }

                    // Find next run time
                    String nextDate = dataDetails.FindNextRunTime(sMinute, sHour, sDOF, sMonth, sDayOfWeek, StdRunDate, con);
                    //System.out.println(nextDate);
                    //updateNextRunTime
                    String qryNextRun = " update  whatsapp_template_master set lastruntime= sysdate , nextruntime = to_date('" + nextDate + "','yyyy-mm-dd hh24:mi:ss') where template_id =" + itemplateid;
                    //System.out.println(qryNextRun);
                    PreparedStatement psNextRun = con.prepareStatement(qryNextRun);
                    psNextRun.executeUpdate();
                    psNextRun.close();

                } else if (rs1.getString(2).equals("officenote_intimation_ver1")) {
                    int itemplateid = rs1.getInt(1);
                    String sMinute = rs1.getString(4);
                    String sHour = rs1.getString(5);
                    String sDOF = rs1.getString(6);
                    String sMonth = rs1.getString(7);
                    String sDayOfWeek = rs1.getString(8);
                    String StdRunDate = "";
                    if (rs1.getDate(13) != null) {
                        StdRunDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs1.getDate(13));
                    }

                    // Get template parameters
                    String stpara = " select id,template_id,parametername from whatsapp_template_master_para where  template_id = 13 ";
                    ps2 = con.prepareStatement(stpara);
                    rs2 = ps2.executeQuery();
                    ArrayList<TemplateParameterBean> TPBarray = new ArrayList();
                    while (rs2.next()) {
                        TemplateParameterBean TPB = new TemplateParameterBean();
                        TPB.setName(rs2.getString(3));
                        TPBarray.add(TPB);
                    }
                    rs2.close();
                    ps2.close();

                    TemplateBean TB = new TemplateBean();
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<OfficeNoteBean> officeNoteList = dataDetails.OfficeNoteList(con);
                    //WhatsappSendClassNew WSC = new WhatsappSendClassNew(con);
                    if (officeNoteList.size() > 0) {
                        WSC.SendWhatsapp(TB, officeNoteList);
                    }

                    // Find next run time
                    String nextDate = dataDetails.FindNextRunTime(sMinute, sHour, sDOF, sMonth, sDayOfWeek, StdRunDate, con);
                    //System.out.println(nextDate);
                    //updateNextRunTime
                    String qryNextRun = " update  whatsapp_template_master set lastruntime= sysdate , nextruntime = to_date('" + nextDate + "','yyyy-mm-dd hh24:mi:ss') where template_id =" + itemplateid;
                    //System.out.println(qryNextRun);
                    PreparedStatement psNextRun = con.prepareStatement(qryNextRun);
                    psNextRun.executeUpdate();
                    psNextRun.close();

                } else if (rs1.getString(2).equals("order_cancelled_ver2")) {  // Mesage after Order Cancellaton
                    int itemplateid = rs1.getInt(1);
                    String sMinute = rs1.getString(4);
                    String sHour = rs1.getString(5);
                    String sDOF = rs1.getString(6);
                    String sMonth = rs1.getString(7);
                    String sDayOfWeek = rs1.getString(8);
                    String StdRunDate = "";
                    if (rs1.getDate(13) != null) {
                        StdRunDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs1.getDate(13));
                    }

                    // Get template parameters
                    String stpara = " select id,template_id,parametername from whatsapp_template_master_para where  template_id = 14 ";
                    ps2 = con.prepareStatement(stpara);
                    rs2 = ps2.executeQuery();
                    ArrayList<TemplateParameterBean> TPBarray = new ArrayList();
                    while (rs2.next()) {
                        TemplateParameterBean TPB = new TemplateParameterBean();
                        TPB.setName(rs2.getString(3));
                        TPBarray.add(TPB);
                    }
                    rs2.close();
                    ps2.close();

                    TemplateBean TB = new TemplateBean();
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<CancelledOrder_removalBean> CancelOrderList = dataDetails.CancelledOrderDetails(con);
                    //WhatsappSendClassNew WSC = new WhatsappSendClassNew(con);
                    if (CancelOrderList.size() > 0) {
                        WSC.SendWhatsapp(TB, CancelOrderList);
                    }

                    // Find next run time
                    String nextDate = dataDetails.FindNextRunTime(sMinute, sHour, sDOF, sMonth, sDayOfWeek, StdRunDate, con);
                    //System.out.println(nextDate);
                    //updateNextRunTime
                    String qryNextRun = " update  whatsapp_template_master set lastruntime= sysdate , nextruntime = to_date('" + nextDate + "','yyyy-mm-dd hh24:mi:ss') where template_id =" + itemplateid;
                    //System.out.println(qryNextRun);
                    PreparedStatement psNextRun = con.prepareStatement(qryNextRun);
                    psNextRun.executeUpdate();
                    psNextRun.close();

                } else if (rs1.getString(2).equals("cotton_inward_ver1")) {
                    int itemplateid = rs1.getInt(1);
                    String sMinute = rs1.getString(4);
                    String sHour = rs1.getString(5);
                    String sDOF = rs1.getString(6);
                    String sMonth = rs1.getString(7);
                    String sDayOfWeek = rs1.getString(8);
                    String StdRunDate = "";
                    if (rs1.getDate(13) != null) {
                        StdRunDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs1.getDate(13));
                    }

                    // Get template parameters
                    String stpara = " select id,template_id,parametername from whatsapp_template_master_para where  template_id = 15 ";
                    ps2 = con.prepareStatement(stpara);
                    rs2 = ps2.executeQuery();
                    ArrayList<TemplateParameterBean> TPBarray = new ArrayList();
                    while (rs2.next()) {
                        TemplateParameterBean TPB = new TemplateParameterBean();
                        TPB.setName(rs2.getString(3));
                        TPBarray.add(TPB);
                    }
                    rs2.close();
                    ps2.close();

                    TemplateBean TB = new TemplateBean();
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<CottonInwardBean> CottonList = dataDetails.CottonInwardList(con);
                    if (CottonList.size() > 0) {
                        if (WSC.SendWhatsappNew(TB, CottonList)) {
                            dataDetails.CloseCottonInwardList(con);
                        }
                    }
                    // Find next run time
                    String nextDate = dataDetails.FindNextRunTime(sMinute, sHour, sDOF, sMonth, sDayOfWeek, StdRunDate, con);
                    //System.out.println(nextDate);
                    //updateNextRunTime
                    String qryNextRun = " update  whatsapp_template_master set lastruntime= sysdate , nextruntime = to_date('" + nextDate + "','yyyy-mm-dd hh24:mi:ss') where template_id =" + itemplateid;
                    //System.out.println(qryNextRun);
                    PreparedStatement psNextRun = con.prepareStatement(qryNextRun);
                    psNextRun.executeUpdate();
                    psNextRun.close();
                } else if (rs1.getString(2).equals("rewind_unitpending_")) {  // Mesage after Order Cancellaton
                    int itemplateid = rs1.getInt(1);
                    String sMinute = rs1.getString(4);
                    String sHour = rs1.getString(5);
                    String sDOF = rs1.getString(6);
                    String sMonth = rs1.getString(7);
                    String sDayOfWeek = rs1.getString(8);
                    String StdRunDate = "";
                    if (rs1.getDate(13) != null) {
                        StdRunDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs1.getDate(13));
                    }

                    // Get template parameters
                    String stpara = " select id,template_id,parametername from whatsapp_template_master_para where  template_id = 16 ";
                    ps2 = con.prepareStatement(stpara);
                    rs2 = ps2.executeQuery();
                    ArrayList<TemplateParameterBean> TPBarray = new ArrayList();
                    while (rs2.next()) {
                        TemplateParameterBean TPB = new TemplateParameterBean();
                        TPB.setName(rs2.getString(3));
                        TPBarray.add(TPB);
                    }
                    rs2.close();
                    ps2.close();

                    TemplateBean TB = new TemplateBean();
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<Rewinding_UnitPendingBean> RewindingList = dataDetails.RewindingPendingAtUnitList(con);
                    if (RewindingList.size() > 0) {
                        WSC.SendWhatsappNew(TB, RewindingList);
                    }
                    // Find next run time
                    String nextDate = dataDetails.FindNextRunTime(sMinute, sHour, sDOF, sMonth, sDayOfWeek, StdRunDate, con);
                    //System.out.println(nextDate);
                    //updateNextRunTime
                    String qryNextRun = " update  whatsapp_template_master set lastruntime= sysdate , nextruntime = to_date('" + nextDate + "','yyyy-mm-dd hh24:mi:ss') where template_id =" + itemplateid;
                    //System.out.println(qryNextRun);
                    PreparedStatement psNextRun = con.prepareStatement(qryNextRun);
                    psNextRun.executeUpdate();
                    psNextRun.close();
                } else if (rs1.getString(2).equals("order_approval_ver1")) {  // Mesage after Order Cancellaton
                    int itemplateid = rs1.getInt(1);
                    String sMinute = rs1.getString(4);
                    String sHour = rs1.getString(5);
                    String sDOF = rs1.getString(6);
                    String sMonth = rs1.getString(7);
                    String sDayOfWeek = rs1.getString(8);
                    String StdRunDate = "";
                    if (rs1.getDate(13) != null) {
                        StdRunDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs1.getDate(13));
                    }

                    // Get template parameters
                    String stpara = " select id,template_id,parametername from whatsapp_template_master_para where  template_id =  " + itemplateid;
                    ps2 = con.prepareStatement(stpara);
                    rs2 = ps2.executeQuery();
                    ArrayList<TemplateParameterBean> TPBarray = new ArrayList();
                    while (rs2.next()) {
                        TemplateParameterBean TPB = new TemplateParameterBean();
                        TPB.setName(rs2.getString(3));
                        TPBarray.add(TPB);
                    }
                    rs2.close();
                    ps2.close();

                    TemplateBean TB = new TemplateBean();
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<YarnOrderApprovalBean> YOAList = dataDetails.YarnOrderAppovalList(con);
                    if (YOAList.size() > 0) {
                        if (WSC.SendWhatsappNew(TB, YOAList)) {
                            dataDetails.CloseYarnOrderClosingList(con);
                        }
                    }
                    // Find next run time
                    String nextDate = dataDetails.FindNextRunTime(sMinute, sHour, sDOF, sMonth, sDayOfWeek, StdRunDate, con);
                    //System.out.println(nextDate);
                    //updateNextRunTime
                    String qryNextRun = " update  whatsapp_template_master set lastruntime= sysdate , nextruntime = to_date('" + nextDate + "','yyyy-mm-dd hh24:mi:ss') where template_id =" + itemplateid;
                    //System.out.println(qryNextRun);
                    PreparedStatement psNextRun = con.prepareStatement(qryNextRun);
                    psNextRun.executeUpdate();
                    psNextRun.close();
                } else if (rs1.getString(2).equals("daily_work_list_ver2")) {
                    int itemplateid = rs1.getInt(1);
                    String sMinute = rs1.getString(4);
                    String sHour = rs1.getString(5);
                    String sDOF = rs1.getString(6);
                    String sMonth = rs1.getString(7);
                    String sDayOfWeek = rs1.getString(8);
                    String StdRunDate = "";
                    if (rs1.getDate(13) != null) {
                        StdRunDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs1.getDate(13));
                    }

                    // Get template parameters
                    String stpara = " select id,template_id,parametername from whatsapp_template_master_para where  template_id =  " + itemplateid;
                    ps2 = con.prepareStatement(stpara);
                    rs2 = ps2.executeQuery();
                    ArrayList<TemplateParameterBean> TPBarray = new ArrayList();
                    while (rs2.next()) {
                        TemplateParameterBean TPB = new TemplateParameterBean();
                        TPB.setName(rs2.getString(3));
                        TPBarray.add(TPB);
                    }
                    rs2.close();
                    ps2.close();

                    TemplateBean TB = new TemplateBean();
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<PendingWorkBean> WPList = dataDetails.WorkPendingList(con);
                    if (WPList.size() > 0) {
                        WSC.SendWhatsappNew(TB, WPList);
                    }
                    // Find next run time
                    String nextDate = dataDetails.FindNextRunTime(sMinute, sHour, sDOF, sMonth, sDayOfWeek, StdRunDate, con);
                    //System.out.println(nextDate);
                    //updateNextRunTime
                    String qryNextRun = " update  whatsapp_template_master set lastruntime= sysdate , nextruntime = to_date('" + nextDate + "','yyyy-mm-dd hh24:mi:ss') where template_id =" + itemplateid;
                    //System.out.println(qryNextRun);
                    PreparedStatement psNextRun = con.prepareStatement(qryNextRun);
                    psNextRun.executeUpdate();
                    psNextRun.close();
                } else if (rs1.getString(2).equals("dyeing_canteen_strength_ver1")) {  // Mesage after Order Cancellaton
                    int itemplateid = rs1.getInt(1);
                    String sMinute = rs1.getString(4);
                    String sHour = rs1.getString(5);
                    String sDOF = rs1.getString(6);
                    String sMonth = rs1.getString(7);
                    String sDayOfWeek = rs1.getString(8);
                    String StdRunDate = "";
                    if (rs1.getDate(13) != null) {
                        StdRunDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs1.getDate(13));
                    }

                    // Get template parameters
                    String stpara = " select id,template_id,parametername from whatsapp_template_master_para where  template_id =  " + itemplateid;
                    ps2 = con.prepareStatement(stpara);
                    rs2 = ps2.executeQuery();
                    ArrayList<TemplateParameterBean> TPBarray = new ArrayList();
                    while (rs2.next()) {
                        TemplateParameterBean TPB = new TemplateParameterBean();
                        TPB.setName(rs2.getString(3));
                        TPBarray.add(TPB);
                    }
                    rs2.close();
                    ps2.close();

                    TemplateBean TB = new TemplateBean();
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<DyeingCateenStrengthBean> WPList = dataDetails.DyeingCanteenStrength(con);  // Last 3 days distinct details
                    System.out.println(WPList.size());
                    if (WPList.size() > 0) {
                        WSC.SendWhatsappNew(TB, WPList);
                    }
                    // Find next run time
                    String nextDate = dataDetails.FindNextRunTime(sMinute, sHour, sDOF, sMonth, sDayOfWeek, StdRunDate, con);
                    //System.out.println(nextDate);
                    //updateNextRunTime
                    String qryNextRun = " update  whatsapp_template_master set lastruntime= sysdate , nextruntime = to_date('" + nextDate + "','yyyy-mm-dd hh24:mi:ss') where template_id =" + itemplateid;
                    //System.out.println(qryNextRun);
                    PreparedStatement psNextRun = con.prepareStatement(qryNextRun);
                    psNextRun.executeUpdate();
                    psNextRun.close();
                } else if (rs1.getString(2).equals("wind_invoice_auth_pending_ia")) {
                    int itemplateid = rs1.getInt(1);
                    String sMinute = rs1.getString(4);
                    String sHour = rs1.getString(5);
                    String sDOF = rs1.getString(6);
                    String sMonth = rs1.getString(7);
                    String sDayOfWeek = rs1.getString(8);
                    String StdRunDate = "";
                    if (rs1.getDate(13) != null) {
                        StdRunDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs1.getDate(13));
                    }

                    // Get template parameters
                    String stpara = " select id,template_id,parametername from whatsapp_template_master_para where  template_id =  " + itemplateid;
                    ps2 = con.prepareStatement(stpara);
                    rs2 = ps2.executeQuery();
                    ArrayList<TemplateParameterBean> TPBarray = new ArrayList();
                    while (rs2.next()) {
                        TemplateParameterBean TPB = new TemplateParameterBean();
                        TPB.setName(rs2.getString(3));
                        TPBarray.add(TPB);
                    }
                    rs2.close();
                    ps2.close();

                    TemplateBean TB = new TemplateBean();
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<WindMillPendingInvoiceBean> PIList = dataDetails.PendingInvoiceCount(con);
                    if (PIList.size() > 0) {
                        WSC.SendWhatsappNew(TB, PIList);
                    }
                    // Find next run time
                    String nextDate = dataDetails.FindNextRunTime(sMinute, sHour, sDOF, sMonth, sDayOfWeek, StdRunDate, con);
                    //System.out.println(nextDate);
                    //updateNextRunTime
                    String qryNextRun = " update  whatsapp_template_master set lastruntime= sysdate , nextruntime = to_date('" + nextDate + "','yyyy-mm-dd hh24:mi:ss') where template_id =" + itemplateid;
                    //System.out.println(qryNextRun);
                    PreparedStatement psNextRun = con.prepareStatement(qryNextRun);
                    psNextRun.executeUpdate();
                    psNextRun.close();
                } else if (rs1.getString(2).equals("recurring_pending_ver1")) {
                    int itemplateid = rs1.getInt(1);
                    String sMinute = rs1.getString(4);
                    String sHour = rs1.getString(5);
                    String sDOF = rs1.getString(6);
                    String sMonth = rs1.getString(7);
                    String sDayOfWeek = rs1.getString(8);
                    String StdRunDate = "";
                    if (rs1.getDate(13) != null) {
                        StdRunDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs1.getDate(13));
                    }

                    // Get template parameters
                    String stpara = " select id,template_id,parametername from whatsapp_template_master_para where  template_id =  " + itemplateid;
                    ps2 = con.prepareStatement(stpara);
                    rs2 = ps2.executeQuery();
                    ArrayList<TemplateParameterBean> TPBarray = new ArrayList();
                    while (rs2.next()) {
                        TemplateParameterBean TPB = new TemplateParameterBean();
                        TPB.setName(rs2.getString(3));
                        TPBarray.add(TPB);
                    }
                    rs2.close();
                    ps2.close();

                    TemplateBean TB = new TemplateBean();
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<RecurringWorkPendingBean> RWPList = dataDetails.PendingRecurringWork(con);
                    if (RWPList.size() > 0) {
                        WSC.SendWhatsappNew(TB, RWPList);
                    }
                    // Find next run time
                    String nextDate = dataDetails.FindNextRunTime(sMinute, sHour, sDOF, sMonth, sDayOfWeek, StdRunDate, con);
                    //System.out.println(nextDate);
                    //updateNextRunTime
                    String qryNextRun = " update  whatsapp_template_master set lastruntime= sysdate , nextruntime = to_date('" + nextDate + "','yyyy-mm-dd hh24:mi:ss') where template_id =" + itemplateid;
                    //System.out.println(qryNextRun);
                    PreparedStatement psNextRun = con.prepareStatement(qryNextRun);
                    psNextRun.executeUpdate();
                    psNextRun.close();
                } else if (rs1.getString(2).equals("ward_pf_alert_ver1")) {
                    int itemplateid = rs1.getInt(1);
                    String sMinute = rs1.getString(4);
                    String sHour = rs1.getString(5);
                    String sDOF = rs1.getString(6);
                    String sMonth = rs1.getString(7);
                    String sDayOfWeek = rs1.getString(8);
                    String StdRunDate = "";
                    if (rs1.getDate(13) != null) {
                        StdRunDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs1.getDate(13));
                    }

                    // Get template parameters
                    String stpara = " select id,template_id,parametername from whatsapp_template_master_para where  template_id =  " + itemplateid;
                    ps2 = con.prepareStatement(stpara);
                    rs2 = ps2.executeQuery();
                    ArrayList<TemplateParameterBean> TPBarray = new ArrayList();
                    while (rs2.next()) {
                        TemplateParameterBean TPB = new TemplateParameterBean();
                        TPB.setName(rs2.getString(3));
                        TPBarray.add(TPB);
                    }
                    rs2.close();
                    ps2.close();

                    TemplateBean TB = new TemplateBean();
                    TB.setTPBList(TPBarray);
                    TB.setTemplate_name(rs1.getString(2));
                    TB.setBroadcast_name(rs1.getString(3));
                    ArrayList<PF_Warden_AlertBean> PFAlertList = dataDetails.PFAlertJob(con);
                    if (PFAlertList.size() > 0) {
                        WSC.SendWhatsappNew(TB, PFAlertList);
                    }
                    // Find next run time
                    String nextDate = dataDetails.FindNextRunTime(sMinute, sHour, sDOF, sMonth, sDayOfWeek, StdRunDate, con);
                    //System.out.println(nextDate);
                    //updateNextRunTime
                    String qryNextRun = " update  whatsapp_template_master set lastruntime= sysdate , nextruntime = to_date('" + nextDate + "','yyyy-mm-dd hh24:mi:ss') where template_id =" + itemplateid;
                    //System.out.println(qryNextRun);
                    PreparedStatement psNextRun = con.prepareStatement(qryNextRun);
                    psNextRun.executeUpdate();
                    psNextRun.close();
                }
            }
            ps1.close();
            rs1.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                ps1.close();
                ps2.close();
                rs1.close();
                rs2.close();
            } catch (Exception ee) {
            }
        }
    }

    public int getCurrnetTime(Connection con) {
        StringBuilder SB = new StringBuilder();
        SB.append(" select code from whatsapp_time_config ");
        SB.append(" where to_date(to_char(sysdate,'HH24:MI'),'HH24:MI') between to_date(daystarttime,'HH24:MI') and  ");
        SB.append(" to_date(dayendtime,'HH24:MI') ");
        int iCode = 0;
        ResultSet rs = null;
        try {
            PreparedStatement ps = con.prepareStatement(SB.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                iCode = rs.getInt(1);
            }
            ps.close();
            rs.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return iCode;
    }
}
