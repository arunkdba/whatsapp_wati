/*
 * Dynamic class to send whatsapp messages
 * Templaste Parameter table has all templates parameter / Some template hava 1 parameter , some have more than 1 
 * This parameter dynanically received. then it will be passed to the messages receipt object to get the value for the parameter 
 * ##  find and read
 */
// ## Here class reflection method used to get value of the variable which is a string
// https://www.geeksforgeeks.org/reflection-in-java/
// TPB.getName() give you field name 
// field.get(GB) give you value from that field name
//System.out.println(GB.getClass().getDeclaredField(TPB.getName()));  java.lang.String whatsappprogram.GRNMsgBean.days / java.lang.String whatsappprogram.GRNMsgBean.days
//System.out.println(TPB.getName());    days  / Nos     #### received values for 2 interations #### 
//System.out.println(field.toString()); java.lang.String whatsappprogram.GRNMsgBean.days / java.lang.String whatsappprogram.GRNMsgBean.days
//System.out.println(field.getName());  days / nos 
//System.out.println(field.get(GB));    2 / 6
package whatsappprogram;

import whatsappprogram.basicbeans.GRNMsgBean;
import whatsappprogram.basicbeans.TemplateParameterBean;
import whatsappprogram.basicbeans.AuthBean;
import whatsappprogram.basicbeans.TemplateBean;
import whatsappprogram.basic.WhatsappAuthorization;
import java.sql.Connection;
import java.util.ArrayList;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import java.lang.reflect.Field;
import whatsappprogram.Beans.ExcessTimeMsgBean;

/**
 *
 * @author admin
 */
public class WhatsappSendClass {

    static Connection con;

    WhatsappSendClass(Connection con) {
        this.con = con;
    }

    public static void SendWhatsapp(TemplateBean tb, Object object, Class classname) {
        try {
            /*
             Body variables
             1. Template Name
             2. Broadcastname
             3. Parameter array list
             Media type variable
             1. mediatype
             Request Variables
             1. resource path
             2. mobile no - optional 
             3. 
            **/
            //ArrayList<GRNMsgBean> GRNList = (ArrayList< GRNMsgBean>) object;
            ArrayList<Object> List = (ArrayList<Object>) object;
            System.out.println("list size " + List.size());

            AuthBean authBean = WhatsappAuthorization.getAuth();

            // Create shared instance with custom settings.
            //OkHttpClient client = new OkHttpClient() this also create instance
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            // Contenttype of the HTTP request
            MediaType mediaType = MediaType.parse("application/json");

            for (Object ob : List) {
                String sMobileNo = "";
                String sParameters = "[";
                boolean morethan1parameter = false;

                if (ob instanceof ExcessTimeMsgBean) {
                    sMobileNo = ((ExcessTimeMsgBean) ob).getMobileno();
                } else if (ob instanceof GRNMsgBean) {
                    sMobileNo = ((GRNMsgBean) ob).getMobileno();
                }
                System.out.println("mobile :" + sMobileNo);
                if (ob instanceof ExcessTimeMsgBean) {
                    for (TemplateParameterBean TPB : tb.getTPBList()) {
                        if (morethan1parameter) {
                            sParameters += ",";
                        }
                        Field field = ((ExcessTimeMsgBean) ob).getClass().getDeclaredField(TPB.getName());  // From TemplateParameterBean
                        String FieldName = field.getName();
                        String FieldValue = (String) (field.get(((ExcessTimeMsgBean) ob)));   //  GRNMsgBean has days and Nos field
                        // [{'name':'days', 'value':'2'}]  Array configuration
                        sParameters += "{'name':'" + FieldName + "','value':'" + FieldValue + "'}";
                        morethan1parameter = true;
                    }
                } else if (ob instanceof GRNMsgBean) {
                    for (TemplateParameterBean TPB : tb.getTPBList()) {
                        if (morethan1parameter) {
                            sParameters += ",";
                        }
                        Field field = ((GRNMsgBean) ob).getClass().getDeclaredField(TPB.getName());  // From TemplateParameterBean
                        String FieldName = field.getName();
                        String FieldValue = (String) (field.get(((GRNMsgBean) ob)));   //  GRNMsgBean has days and Nos field
                        // [{'name':'days', 'value':'2'}]  Array configuration
                        sParameters += "{'name':'" + FieldName + "','value':'" + FieldValue + "'}";
                        morethan1parameter = true;
                    }
                }

                sParameters += "]";
                System.out.println(sParameters);
                RequestBody body = RequestBody.create(mediaType, "{\n    \"template_name\": \"" + tb.template_name + "\",\n    \"broadcast_name\": \"" + tb.broadcast_name + "\",\n   "
                        + " \"parameters\": \" " + sParameters + " \"\n}");
                System.out.println("{\n    \"template_name\": \"" + tb.template_name + "\",\n    \"broadcast_name\": \"" + tb.broadcast_name + "\",\n   "
                        + " \"parameters\": \" " + sParameters + " \"\n}");

                // Create request with body
                Request request = new Request.Builder()
                        //.url("https://live-server-6241.wati.io/api/v1/sendTemplateMessage/919442240787")
                        //.url("" + authBean.getUrl() + "sendTemplateMessage/919442240787")
                        .url("" + authBean.getUrl() + "sendTemplateMessage/" + sMobileNo + "")
                        .method("POST", body)
                        .addHeader("Authorization", "" + authBean.getAuthtoken() + "")
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Cookie", "affinity=1639028173.342.141886.905232")
                        .build();
                // Get response by executing request using the client
                //Response response = client.newCall(request).execute();
                //System.out.println(response);
                //System.out.println(response.body().string());

            }
            /*
            AuthBean authBean = WhatsappAuthorization.getAuth(con);

            // Create shared instance with custom settings.
            //OkHttpClient client = new OkHttpClient() this also create instance
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            // Contenttype of the HTTP request
            MediaType mediaType = MediaType.parse("application/json");
            // create body with specified mediatypec
            //RequestBody body = RequestBody.create(mediaType, "{\n    \"template_name\": \"order_hold_cancel_ver3\",\n    \"broadcast_name\": \"order_update\",\n   "
            //      + " \"parameters\": \"[{'name':'orderno', 'value':'LM125051'}]\"\n}");
            RequestBody body = RequestBody.create(mediaType, "{\n    \"template_name\": \""+tb.template_name+"\",\n    \""+tb.broadcast_name+"\": \"order_update\",\n   "
                    + " \"parameters\": \"[{'name':'orderno', 'value':'LM125051'}]\"\n}");

            // Create request with body
            Request request = new Request.Builder()
                    //.url("https://live-server-6241.wati.io/api/v1/sendTemplateMessage/919442240787")
                    .url("" + authBean.getUrl() + "sendTemplateMessage/919442240787")
                    .method("POST", body)
                    .addHeader("Authorization", "" + authBean.getAuthtoken() + "")
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Cookie", "affinity=1639028173.342.141886.905232")
                    .build();
            // Get response by executing request using the client
            Response response = client.newCall(request).execute();
             */
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
