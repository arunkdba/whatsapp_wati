package whatsappprogram.basic;

import java.sql.*;

public class OracleConnection {

    static Connection dbConnection = null;
    static final String username = "admin";
    static final String password = "admin";
    static final String dbHost = "172.16.2.28";
    static final String dbName = "arun";
    static final int ServerPort = 1521;
    static String Server = "oracle.jdbc.OracleDriver";
    static String DSN = "jdbc:oracle:thin:@172.16.2.28:1521:arun";

    static Connection con = null;

    public static Connection getConnection() {

        //System.out.println("connection details " + dbConnection);
        if (dbConnection != null) {
            System.out.println("null conn " + dbConnection);
            return dbConnection;
        } else {
            //System.out.println("for new conn1 " + username + " " + password + " " + dbHost + " " + dbName + " " + ServerPort);
            return getConnection(username, password, dbHost, dbName, ServerPort);
        }
    }

    public static Connection getConnection(String sName, String sPassWord, String sDBHost, String sDBName, int iServerPort) {
        try {
            Class.forName(Server);
            con = DriverManager.getConnection(DSN,sName, sPassWord);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return con;
    }

    public static void closeConnection(Connection con) {
        try{
            con.close();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }
}
