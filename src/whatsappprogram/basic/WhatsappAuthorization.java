/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package whatsappprogram.basic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import whatsappprogram.basicbeans.AuthBean;
import static whatsappprogram.Ver_3.ThreadJava3.con;

/**
 *
 * @author admin
 */
public class WhatsappAuthorization {

    public static AuthBean getAuth() {
        PreparedStatement ps, ps1;
        ResultSet rs, rs1;
        //System.out.println(con.toString());
        // tables contains wati url and authorization token
        String st = "select url,authorization from whatsapp_config";
        AuthBean authBean = new AuthBean();
        try {
            ps = con.prepareStatement(st);
            rs = ps.executeQuery();
            while (rs.next()) {
                authBean.setUrl(rs.getString(1));
                authBean.setAuthtoken(rs.getString(2));
            }
            rs.close();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return authBean;
    }
}
