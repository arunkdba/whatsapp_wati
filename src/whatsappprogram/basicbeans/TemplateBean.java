
package whatsappprogram.basicbeans;

import java.util.ArrayList;

public class TemplateBean {
    public String template_name,broadcast_name;
    ArrayList<TemplateParameterBean> TPBList ;

    public ArrayList<TemplateParameterBean> getTPBList() {
        return TPBList;
    }

    public void setTPBList(ArrayList<TemplateParameterBean> TPBList) {
        this.TPBList = TPBList;
    }

   
    

    public String getTemplate_name() {
        return template_name;
    }

    public void setTemplate_name(String template_name) {
        this.template_name = template_name;
    }

    public String getBroadcast_name() {
        return broadcast_name;
    }

    public void setBroadcast_name(String broadcast_name) {
        this.broadcast_name = broadcast_name;
    }


}
